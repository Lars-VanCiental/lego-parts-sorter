<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Application;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Storage;

interface WritableStorageSpace
{
    public function createStorage(string $storageName, string $storageDescription, Matcher $matcher, Part ...$storedParts): Storage;
}
