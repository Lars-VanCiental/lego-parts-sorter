<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Application;

use LVC\LegoSorter\Domain\SortingResult;
use LVC\LegoSorter\Domain\Storage;
use LVC\LegoSorter\Domain\StoringPlan;
use LVC\LegoSorter\Domain\UnsortedPartsStack;

class StoreSortedPartsAction
{
    private StorageSpace $storageSpace;

    public function __construct(
        StorageSpace $storageSpace,
    ) {
        $this->storageSpace = $storageSpace;
    }

    public function __invoke(
        SortingResult $sortingResult,
        bool $emptyStorages
    ): UnsortedPartsStack {
        $storages = $this->storageSpace->takeOutStorages();

        if ($emptyStorages === true) {
            $this->emptyStorages($sortingResult, ...$storages);
        }

        foreach ($sortingResult->getStoringPlans() as $storingPlan) {
            $this->applyStoringPlan($storingPlan, ...$storages);
        }

        $this->storageSpace->putAwayStorages(...$storages);

        return $sortingResult->getUnsortedPartsStack();
    }

    private function emptyStorages(SortingResult $sortingResult, Storage ...$storages): void
    {
        foreach ($storages as $storage) {
            foreach ($storage->empty()->getParts() as $part) {
                $sortingResult->stackPart($part);
            }
        }
    }

    private function applyStoringPlan(StoringPlan $storingPlan, Storage ...$storages): void
    {
        foreach ($storages as $storage) {
            if ($storingPlan->getStorage()->getName() === $storage->getName()) {
                $storage->storePart($storingPlan->getPart());

                return;
            }
        }

        throw new \RuntimeException('Storage plan given toward a missing storage "'.$storingPlan->getStorage()->getName().'"');
    }
}
