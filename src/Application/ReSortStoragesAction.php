<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Application;

use LVC\LegoSorter\Domain\Matcher\NullMatcher;
use LVC\LegoSorter\Domain\Sorter;
use LVC\LegoSorter\Domain\SortingResult;

class ReSortStoragesAction
{
    private StorageSpace $storageSpace;

    public function __construct(
        StorageSpace $storageSpace,
    ) {
        $this->storageSpace = $storageSpace;
    }

    public function __invoke(
    ): SortingResult {
        $sortingResult = new SortingResult();
        $storages = $this->storageSpace->takeOutStorages();
        $sorter = new Sorter(new NullMatcher(), $sortingResult, ...$storages);

        foreach ($storages as $storage) {
            foreach ($storage->getParts() as $part) {
                $sorter->sortPart($part);
            }
        }

        return $sortingResult;
    }
}
