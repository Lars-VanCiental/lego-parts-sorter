<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Application;

use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Sorter;
use LVC\LegoSorter\Domain\SortingResult;

class SortSetAction
{
    private Matcher $ignoringMatcher;
    private StorageSpace $storageSpace;
    private Catalog $catalog;
    private bool $ignoreMissingSets;

    public function __construct(
        Matcher $ignoringMatcher,
        StorageSpace $storageSpace,
        Catalog $catalog,
        bool $ignoreMissingSets
    ) {
        $this->ignoringMatcher = $ignoringMatcher;
        $this->storageSpace = $storageSpace;
        $this->catalog = $catalog;
        $this->ignoreMissingSets = $ignoreMissingSets;
    }

    public function __invoke(
        string ...$setNumbers
    ): SortingResult {
        $sortingResult = new SortingResult();
        $storages = $this->storageSpace->takeOutStorages();
        $sorter = new Sorter($this->ignoringMatcher, $sortingResult, ...$storages);

        foreach ($setNumbers as $setNumber) {
            $set = $this->catalog->getSet($setNumber);
            if ($set === null) {
                if ($this->ignoreMissingSets === true) {
                    continue;
                }

                throw new \UnexpectedValueException('No set found with given set number "'.$setNumber.'".');
            }

            $sorter->sortSet($set);
        }

        return $sortingResult;
    }
}
