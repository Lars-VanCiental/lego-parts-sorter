<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Application;

use LVC\LegoSorter\Domain\Storage;

interface StorageSpace
{
    /**
     * @return Storage[]
     */
    public function takeOutStorages(): array;

    public function putAwayStorages(Storage ...$storages): void;
}
