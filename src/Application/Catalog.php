<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Application;

use LVC\LegoSorter\Domain\Lego\Category;
use LVC\LegoSorter\Domain\Lego\Color;
use LVC\LegoSorter\Domain\Lego\Set;

interface Catalog
{
    /**
     * @return array<int, Category>
     */
    public function getCategories(): array;

    /**
     * @return array<int, Color>
     */
    public function getColors(): array;

    public function getSet(string $setNumber): ?Set;
}
