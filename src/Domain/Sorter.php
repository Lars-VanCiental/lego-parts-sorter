<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Lego\Set;
use LVC\LegoSorter\Domain\Matcher\NullMatcher;

class Sorter
{
    private Matcher|NullMatcher $ignoringMatcher;
    private SortingResult $sortingResult;
    /** @var Storage[] */
    private array $storages;

    public function __construct(
        Matcher $ignoringMatcher,
        SortingResult $sortingResult,
        Storage ...$storages
    ) {
        $this->ignoringMatcher = $ignoringMatcher;
        $this->sortingResult = $sortingResult;
        $this->storages = $storages;
    }

    public function sortSet(Set $set): void
    {
        foreach ($set->getParts() as $part) {
            $this->sortPart($part);
        }
    }

    public function sortPart(Part $part): void
    {
        if ($this->ignoringMatcher->accept($part)) {
            $this->sortingResult->ignorePart($part);

            return;
        }

        foreach ($this->storages as $storage) {
            if ($storage->getMatcher()->accept($part)) {
                $this->sortingResult->planPartStorage($part, $storage);

                return;
            }
        }

        $this->sortingResult->stackPart($part);
    }
}
