<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain;

use LVC\LegoSorter\Domain\Lego\Part;

interface Storage
{
    public function getName(): string;

    public function getDescription(): string;

    public function getMatcher(): Matcher;

    public function empty(): UnsortedPartsStack;

    public function storePart(Part $part): void;

    /**
     * @return Part[]
     */
    public function getParts(): array;
}
