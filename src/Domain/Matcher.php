<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain;

use LVC\LegoSorter\Domain\Lego\Part;

interface Matcher
{
    public function accept(Part $part): bool;
}
