<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain;

use LVC\LegoSorter\Domain\Lego\Part;

final class StoringPlan
{
    private Part $part;
    private Storage $storage;

    public function __construct(
        Part $part,
        Storage $storage
    ) {
        $this->part = $part;
        $this->storage = $storage;
    }

    public function getPart(): Part
    {
        return $this->part;
    }

    public function getStorage(): Storage
    {
        return $this->storage;
    }
}
