<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain;

use LVC\LegoSorter\Domain\Lego\Part;

final class SortingResult
{
    private IgnoredPartsList $ignoredPartsList;
    /** @var array<int, StoringPlan> */
    private array $storingPlans;
    private UnsortedPartsStack $unsortedPartsStack;

    public function __construct()
    {
        $this->ignoredPartsList = new IgnoredPartsList();
        $this->storingPlans = [];
        $this->unsortedPartsStack = new UnsortedPartsStack();
    }

    public function planPartStorage(Part $part, Storage $storage): void
    {
        $this->storingPlans[] = new StoringPlan($part, $storage);
    }

    public function stackPart(Part $part): void
    {
        $this->unsortedPartsStack->stackPart($part);
    }

    public function ignorePart(Part $part): void
    {
        $this->ignoredPartsList->ignorePart($part);
    }

    /**
     * @return array<int, StoringPlan>
     */
    public function getStoringPlans(): array
    {
        return $this->storingPlans;
    }

    public function getUnsortedPartsStack(): UnsortedPartsStack
    {
        return $this->unsortedPartsStack;
    }

    public function getIgnoredPartsList(): IgnoredPartsList
    {
        return $this->ignoredPartsList;
    }
}
