<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain;

use LVC\LegoSorter\Domain\Lego\Part;

final class IgnoredPartsList
{
    /** @var Part[] */
    private array $parts = [];

    public function ignorePart(Part $part): void
    {
        $this->parts[] = $part;
    }

    /**
     * @return Part[]
     */
    public function getParts(): array
    {
        return $this->parts;
    }
}
