<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Lego;

interface Part
{
    public function getPartNumber(): string;

    public function getType(): string;

    public function getColor(): Color;

    public function getCategory(): Category;

    public function isSame(self $part): bool;
}
