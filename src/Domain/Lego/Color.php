<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Lego;

interface Color
{
    public function getId(): string;

    public function getName(): string;
}
