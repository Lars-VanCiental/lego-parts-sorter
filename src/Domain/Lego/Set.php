<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Lego;

interface Set
{
    public function getSetNumber(): string;

    public function getName(): string;

    /**
     * @return Part[]
     */
    public function getParts(): array;
}
