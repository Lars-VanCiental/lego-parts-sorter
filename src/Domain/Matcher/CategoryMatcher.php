<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;

final class CategoryMatcher implements Matcher
{
    private string $categoryName;

    public function __construct(string $categoryName)
    {
        $this->categoryName = $categoryName;
    }

    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    public function accept(Part $part): bool
    {
        return $part->getCategory()->getName() === $this->categoryName;
    }
}
