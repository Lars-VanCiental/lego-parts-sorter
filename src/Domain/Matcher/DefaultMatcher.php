<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;

final class DefaultMatcher implements Matcher
{
    public function accept(Part $part): bool
    {
        return true;
    }
}
