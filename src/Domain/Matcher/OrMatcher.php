<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;

final class OrMatcher implements Matcher
{
    /** @var Matcher[] */
    private array $matchers;

    public function __construct(Matcher ...$matchers)
    {
        $this->matchers = $matchers;
    }

    /**
     * @return Matcher[]
     */
    public function getMatchers(): array
    {
        return $this->matchers;
    }

    public function accept(Part $part): bool
    {
        foreach ($this->matchers as $matcher) {
            if ($matcher->accept($part)) {
                return true;
            }
        }

        return false;
    }
}
