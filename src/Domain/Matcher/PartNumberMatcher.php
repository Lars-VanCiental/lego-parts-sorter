<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;

final class PartNumberMatcher implements Matcher
{
    private string $partNumber;

    public function __construct(string $partNumber)
    {
        $this->partNumber = $partNumber;
    }

    public function getPartNumber(): string
    {
        return $this->partNumber;
    }

    public function accept(Part $part): bool
    {
        return $part->getPartNumber() === $this->partNumber;
    }
}
