<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;

final class ColorMatcher implements Matcher
{
    private string $colorName;

    public function __construct(string $colorName)
    {
        $this->colorName = $colorName;
    }

    public function getColorName(): string
    {
        return $this->colorName;
    }

    public function accept(Part $part): bool
    {
        return $part->getColor()->getName() === $this->colorName;
    }
}
