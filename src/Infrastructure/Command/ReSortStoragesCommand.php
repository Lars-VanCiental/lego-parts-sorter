<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Application\ReSortStoragesAction;
use LVC\LegoSorter\Application\StorageSpace;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ReSortStoragesCommand extends Command
{
    use ConfigurationOptionParser;
    use PrinterFactory;
    use SortingResultHandler;

    protected function configure(): void
    {
        $this->setName('sort:storages');
        $this->setDescription('Sort all your existing storages.');
        $this->setHelp('This command will reset your storage spaces and sort all parts again, according to the (new) configuration given with --config option.');
        $this->addPrinterOptions();

        $this->addConfigurationOption();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->printer = $this->createPrinter($input, $io);

        try {
            /** @var StorageSpace $storageSpace */
            [, $storageSpace, ] = self::parseConfigurationOption($input);
        } catch (\Throwable $throwable) {
            $io->error('An error happened while trying to validate the configuration: '.$throwable->getMessage());

            return 1;
        }

        return $this->sortStorages($storageSpace, $io);
    }

    private function sortStorages(
        StorageSpace $storageSpace,
        SymfonyStyle $io,
    ): int {
        $action = new ReSortStoragesAction($storageSpace);
        $sortingResult = $action();

        switch ($this->handleSortingResult($sortingResult, $storageSpace, $io, true)) {
            case SortingOutcome::Saved:
                $io->success('Storages fully sorted out.');

                return 0;

            case SortingOutcome::Rejected:
            default:
                return 1;
        }
    }
}
