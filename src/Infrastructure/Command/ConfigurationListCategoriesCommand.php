<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Domain\Lego\Category;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConfigurationListCategoriesCommand extends Command
{
    use ConfigurationOptionParser;

    protected function configure(): void
    {
        $this->setName('config:categories');
        $this->setDescription('Display the list of categories.');
        $this->setHelp('This command will print the list of available categories in your catalog to help configure the matchers.');

        $this->addConfigurationOption();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            [$catalog, , ] = self::parseConfigurationOption($input);
        } catch (\Throwable $throwable) {
            $io->error('An error happened while trying to validate the configuration: '.$throwable->getMessage());

            return 1;
        }

        if ($catalog === null) {
            $io->error('In order to list available categories, a catalog must be configured.');

            return 1;
        }

        $categories = $catalog->getCategories();

        $io->section('Displaying the names of '.count($categories).' categories.');
        $io->listing(array_map(fn (Category $category): string => $category->getName(), $categories));

        return 0;
    }
}
