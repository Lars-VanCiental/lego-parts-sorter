<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Application\StoreSortedPartsAction;
use LVC\LegoSorter\Domain\SortingResult;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EmptyStoragesCommand extends Command
{
    use ConfigurationOptionParser;

    private const OPTION_CONFIRM = 'confirm';

    protected function configure(): void
    {
        $this->setName('empty:storages');
        $this->setDescription('Empty all your existing storages.');
        $this->setHelp('This command will empty your storage spaces. Warning this is not reversible!');

        $this->addOption(self::OPTION_CONFIRM, null, InputOption::VALUE_NONE);

        $this->addConfigurationOption();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            /** @var StorageSpace $storageSpace */
            [, $storageSpace, ] = self::parseConfigurationOption($input);
        } catch (\Throwable $throwable) {
            $io->error('An error happened while trying to validate the configuration: '.$throwable->getMessage());

            return 1;
        }

        $io->warning('About to empty your storages, no rollback possible.');

        $confirm = $input->getOption(self::OPTION_CONFIRM);
        if (!$confirm) {
            if (!$io->confirm('Are you sure you want to empty your storages (no rollback possible)?', false)) {
                $io->error('Not going further with emptying your storages without explicit confirmation.');

                return 1;
            }
        }

        return $this->emptyStorages($storageSpace, $io);
    }

    private function emptyStorages(
        StorageSpace $storageSpace,
        SymfonyStyle $io,
    ): int {
        $action = new StoreSortedPartsAction($storageSpace);

        $unstackedPartStack = $action(new SortingResult(), true);
        $numberOfParts = count($unstackedPartStack->getParts());

        $io->success($numberOfParts.' parts have been removed from your storages, that are now empty.');

        return 0;
    }
}
