<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Storage;
use LVC\LegoSorter\Infrastructure\Adapter\CachedCatalog;
use LVC\LegoSorter\Infrastructure\Adapter\CSVCatalog;
use LVC\LegoSorter\Infrastructure\Adapter\RebrickableCatalog;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConfigurationValidatorCommand extends Command
{
    use ConfigurationOptionParser;

    protected function configure(): void
    {
        $this->setName('config:validate');
        $this->setDescription('Validate the given configuration files.');
        $this->setHelp('This command will check that the given configuration files (with the --config option) are valid, and print the result.');

        $this->addConfigurationOption();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            [$catalog, $storageSpace, $ignoringMatcher] = self::parseConfigurationOption($input);
        } catch (\Throwable $throwable) {
            $io->error('An error happened while trying to validate the configuration: '.$throwable->getMessage());

            return 1;
        }

        $io->success('Valid configuration!');
        $this->describeCatalog($io, $catalog);
        $this->describeStorageSpace($io, $storageSpace);
        $this->describeIgnoringMatcher($io, $ignoringMatcher);

        return 0;
    }

    private function describeCatalog(SymfonyStyle $io, ?Catalog $catalog): void
    {
        if ($catalog === null) {
            $io->warning('No catalog provided. Consider changing this if you plan to sort individual sets.');

            return;
        }

        if ($catalog instanceof CachedCatalog) {
            $catalog = $catalog->getCatalog();
        }

        $io->info(
            match (true) {
                $catalog instanceof CSVCatalog => 'Using CSV catalog configured to read sets from directory "'.$catalog->getDirectoryPath().'".',
                $catalog instanceof RebrickableCatalog => 'Using Rebrickable catalog.',
                default => 'Using '.get_class($catalog).' catalog.',
            }
        );
    }

    private function describeStorageSpace(SymfonyStyle $io, StorageSpace $storageSpace): void
    {
        $io->info('Using the following storages:');
        $io->listing(
            array_map(
                fn (Storage $storage) => $storage->getName().(empty($storage->getDescription()) ? '' : ' ('.$storage->getDescription().')').' with matcher '.$this->getMatcherDescription($storage->getMatcher()),
                $storageSpace->takeOutStorages()
            )
        );
    }

    private function describeIgnoringMatcher(SymfonyStyle $io, Matcher $ignoringMatcher): void
    {
        if ($ignoringMatcher instanceof Matcher\NullMatcher) {
            return;
        }

        $io->block(
            'The parts following this matcher will be ignored when sorting:'.$this->getMatcherDescription($ignoringMatcher),
            'WARNING',
            'fg=yellow',
            ' ',
            true
        );
    }

    private function getMatcherDescription(Matcher $matcher): string
    {
        return match (true) {
            $matcher instanceof Matcher\AndMatcher => implode(' && ', array_map(fn (Matcher $nestedMatcher): string => '('.$this->getMatcherDescription($nestedMatcher).')', $matcher->getMatchers())),
            $matcher instanceof Matcher\CategoryMatcher => 'category: '.$matcher->getCategoryName(),
            $matcher instanceof Matcher\ColorMatcher => 'color: '.$matcher->getColorName(),
            $matcher instanceof Matcher\DefaultMatcher => 'yes',
            $matcher instanceof Matcher\NullMatcher => 'no',
            $matcher instanceof Matcher\OrMatcher => implode(' || ', array_map(fn (Matcher $nestedMatcher): string => '('.$this->getMatcherDescription($nestedMatcher).')', $matcher->getMatchers())),
            $matcher instanceof Matcher\PartNumberMatcher => 'number: '.$matcher->getPartNumber(),
            $matcher instanceof Matcher\TypeMatcher => 'type: '.$matcher->getType(),
            default => throw new \InvalidArgumentException('Unknown matcher given.'),
        };
    }
}
