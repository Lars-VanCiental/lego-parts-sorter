<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Domain\Lego\Color;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConfigurationListColorsCommand extends Command
{
    use ConfigurationOptionParser;

    protected function configure(): void
    {
        $this->setName('config:colors');
        $this->setDescription('Display the list of colors.');
        $this->setHelp('This command will print the list of available colors in your catalog to help configure the matchers.');

        $this->addConfigurationOption();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            [$catalog, , ] = self::parseConfigurationOption($input);
        } catch (\Throwable $throwable) {
            $io->error('An error happened while trying to validate the configuration: '.$throwable->getMessage());

            return 1;
        }

        if ($catalog === null) {
            $io->error('In order to list available colors, a catalog must be configured.');

            return 1;
        }

        $colors = $catalog->getColors();

        $io->section('Displaying the names of '.count($colors).' colors.');
        $io->listing(array_map(fn (Color $color): string => $color->getName(), $colors));

        return 0;
    }
}
