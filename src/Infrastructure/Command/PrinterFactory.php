<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Infrastructure\Printer\ConsolePrinter;
use LVC\LegoSorter\Infrastructure\Printer\HtmlFilePrinter;
use LVC\LegoSorter\Infrastructure\Printer\Printer;
use LVC\LegoSorter\Infrastructure\Printer\TemplateEngine;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;

trait PrinterFactory
{
    private function getPrinterOptionName(): string
    {
        return 'printer-format';
    }

    private function getPrinterOptionValueConsole(): string
    {
        return 'console';
    }

    private function getPrinterOptionValueHtml(): string
    {
        return 'html';
    }

    private function getFileOptionName(): string
    {
        return 'printer-output';
    }

    private function addPrinterOptions(): void
    {
        $this->addOption(
            $this->getPrinterOptionName(),
            'f',
            InputOption::VALUE_REQUIRED,
            'Format used for the listing output. Available option are "'.$this->getPrinterOptionValueConsole().'" or "'.$this->getPrinterOptionValueHtml().'".',
            $this->getPrinterOptionValueConsole()
        );
        $this->addOption(
            $this->getFileOptionName(),
            'o',
            InputOption::VALUE_REQUIRED,
            'Optional file name for the report when using another format than console.',
            null
        );
    }

    private function createPrinter(InputInterface $input, SymfonyStyle $io): Printer
    {
        $format = $input->getOption(self::getPrinterOptionName());

        switch ($format) {
            case $this->getPrinterOptionValueConsole():
                return new ConsolePrinter($io);
            case $this->getPrinterOptionValueHtml():
                $filename = $input->getOption($this->getFileOptionName());
                if ($filename !== null && !is_string($filename)) {
                    throw new \InvalidArgumentException('Filename given for the printer output must ba a non empty string.');
                }

                return new HtmlFilePrinter(
                    implode(
                        DIRECTORY_SEPARATOR,
                        [
                            __DIR__,
                            '..',
                            '..',
                            '..',
                            'var',
                            'sorting-results',
                        ]
                    ),
                    new TemplateEngine(
                        implode(
                            DIRECTORY_SEPARATOR,
                            [
                                __DIR__,
                                '..',
                                'Printer',
                                'templates',
                            ]
                        ),
                        '.html',
                    ),
                    $filename
                );
            default:
                throw new \RuntimeException('Invalid printer format given. Valid options are are "'.$this->getPrinterOptionValueConsole().'" or "'.$this->getPrinterOptionValueHtml().'".');
        }
    }
}
