<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

enum SortingOutcome
{
    case Rejected;
    case Saved;
}
