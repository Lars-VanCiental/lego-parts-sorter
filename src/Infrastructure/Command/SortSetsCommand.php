<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Application\SortSetAction;
use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Domain\Matcher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SortSetsCommand extends Command
{
    use ConfigurationOptionParser;
    use PrinterFactory;
    use SortingResultHandler;

    private const ARGUMENT_SETS = 'sets';
    private const OPTION_IGNORE_MISSING_SETS = 'ignore-missing-sets';
    private const OPTION_IGNORE_RESULT = 'draft';

    protected function configure(): void
    {
        $this->setName('sort:sets');
        $this->setDescription('Sort given sets into your existing storages.');
        $this->setHelp('This command will sort all parts from given set, according to the (new) configuration given with --config option.');

        $this->addConfigurationOption();
        $this->addPrinterOptions();
        $this->addArgument(
            self::ARGUMENT_SETS,
            InputArgument::REQUIRED | InputArgument::IS_ARRAY,
            'List of set numbers to sort into your storages.',
            null
        );
        $this->addOption(
            self::OPTION_IGNORE_MISSING_SETS,
            'i',
            InputOption::VALUE_NONE,
            'Sets not found are ignored with this option.'
        );
        $this->addOption(
            self::OPTION_IGNORE_RESULT,
            'd',
            InputOption::VALUE_NONE,
            'Sorting result will immediately be printed according to the given format and not saved.'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->printer = $this->createPrinter($input, $io);

        try {
            /** @var ?Catalog $catalog */
            /** @var StorageSpace $storageSpace */
            /** @var Matcher $ignoringMatcher */
            [$catalog, $storageSpace, $ignoringMatcher] = self::parseConfigurationOption($input);
        } catch (\Throwable $throwable) {
            $io->error('An error happened while trying to validate the configuration: '.$throwable->getMessage());

            return 1;
        }

        if ($catalog === null) {
            $io->error('In order to sort sets, a catalog must be configured.');

            return 1;
        }

        $setsArgument = $input->getArgument(self::ARGUMENT_SETS);
        if (!is_array($setsArgument)) {
            $io->error('At least one set must be given to be sorted.');

            return 1;
        }

        $setNumbers = [];
        foreach ($setsArgument as $setArgument) {
            $setNumbers[] = $setArgument;
        }

        $ignoreMissingSets = $input->hasOption(self::OPTION_IGNORE_MISSING_SETS) && $input->getOption(self::OPTION_IGNORE_MISSING_SETS);
        $ignoreResult = $input->hasOption(self::OPTION_IGNORE_RESULT) && $input->getOption(self::OPTION_IGNORE_RESULT);

        $action = new SortSetAction($ignoringMatcher, $storageSpace, $catalog, $ignoreMissingSets);

        $sortingResult = $action(...$setNumbers);

        if ($ignoreResult) {
            $this->printer->printSortingResult($sortingResult);

            return 0;
        }

        switch ($this->handleSortingResult($sortingResult, $storageSpace, $io, false)) {
            case SortingOutcome::Saved:
                $io->success('Set fully sorted out.');

                return 0;

            case SortingOutcome::Rejected:
            default:
                return 1;
        }
    }
}
