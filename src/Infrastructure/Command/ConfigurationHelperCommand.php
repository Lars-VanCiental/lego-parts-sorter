<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Infrastructure\Configuration\LegoSorterConfiguration;
use Symfony\Component\Config\Definition\Dumper\YamlReferenceDumper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConfigurationHelperCommand extends Command
{
    protected function configure(): void
    {
        $this->setName('config:help');
        $this->setDescription('Dump the configuration file options.');
        $this->setHelp('This command will print all options available for your configuration files given with --config in the other command.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $dumper = new YamlReferenceDumper();

        $io->info($dumper->dump(new LegoSorterConfiguration()));

        return 0;
    }
}
