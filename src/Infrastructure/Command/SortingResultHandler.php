<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Application\StoreSortedPartsAction;
use LVC\LegoSorter\Application\WritableStorageSpace;
use LVC\LegoSorter\Domain\Matcher\NullMatcher;
use LVC\LegoSorter\Domain\SortingResult;
use LVC\LegoSorter\Infrastructure\Printer\Printer;
use Symfony\Component\Console\Style\SymfonyStyle;

trait SortingResultHandler
{
    private Printer $printer;

    private function handleSortingResult(
        SortingResult $sortingResult,
        StorageSpace $storageSpace,
        SymfonyStyle $io,
        bool $emptyStorages
    ): SortingOutcome {
        $this->printer->printSortingResult($sortingResult);

        $unsortedParts = $sortingResult->getUnsortedPartsStack()->getParts();
        if (count($unsortedParts) > 0) {
            $io->warning('There are '.count($unsortedParts).' unsorted parts!');

            $retryChoice = 'Update configuration and try the command again';
            $ignoreChoice = 'Ignore them';
            $saveChoice = 'Save as a new storage';

            $choices = [$ignoreChoice, $retryChoice];
            if ($storageSpace instanceof WritableStorageSpace) {
                $choices[] = $saveChoice;
            }

            $choice = $io->choice('What to do with those parts?', $choices, $retryChoice);
            switch ($choice) {
                case $retryChoice:
                    return SortingOutcome::Rejected;

                case $ignoreChoice:
                    break;

                case $saveChoice:
                    $sortingResult = $this->planUnsortedPartsStackStorage($io, $storageSpace, $sortingResult);

                    if ($io->confirm('Review updated storing plan?', false)) {
                        $this->printer->printSortingResult($sortingResult);
                    }

                    break;

                default:
                    throw new \InvalidArgumentException();
            }
        }

        $saveStoragePlans = $io->confirm('Save planned changes?');
        if ($saveStoragePlans !== true) {
            return SortingOutcome::Rejected;
        }

        $action = new StoreSortedPartsAction($storageSpace);
        $action($sortingResult, $emptyStorages);

        return SortingOutcome::Saved;
    }

    private function planUnsortedPartsStackStorage(
        SymfonyStyle $io,
        StorageSpace $storageSpace,
        SortingResult $sortingResult,
    ): SortingResult {
        if (!$storageSpace instanceof WritableStorageSpace) {
            throw new \InvalidArgumentException('Saving unsorted parts as a new storage is not implemented in this storage space.');
        }

        $unsortedParts = $sortingResult->getUnsortedPartsStack()->getParts();

        /** @var string $storageName */
        $storageName = $io->ask(
            'What name do you want to call the new storage space for the unsorted parts?',
            null,
            function ($storageName): string {
                if (!is_string($storageName) || empty($storageName)) {
                    throw new \RuntimeException('Invalid storage name given.');
                }

                return $storageName;
            }
        );

        try {
            $newStorage = $storageSpace->createStorage($storageName, 'Unsorted parts', new NullMatcher(), ...$unsortedParts);
        } catch (\Exception $e) {
            $io->error('Could not create storage "'.$storageName.'" for the '.count($unsortedParts).' unsorted parts, trying again.');
            $io->comment('Reason is: '.$e->getMessage());

            return $this->planUnsortedPartsStackStorage($io, $storageSpace, $sortingResult);
        }

        $io->success('The '.count($unsortedParts).' unsorted parts will be saved saved in your new storage called "'.$storageName.'".');
        $io->warning('You will have to update your configuration file to make use of this new storage space!');

        $updatedSortingResult = new SortingResult();
        foreach ($sortingResult->getStoringPlans() as $storingPlan) {
            $updatedSortingResult->planPartStorage($storingPlan->getPart(), $storingPlan->getStorage());
        }
        foreach ($unsortedParts as $unsortedPart) {
            $sortingResult->planPartStorage($unsortedPart, $newStorage);
        }

        return $updatedSortingResult;
    }
}
