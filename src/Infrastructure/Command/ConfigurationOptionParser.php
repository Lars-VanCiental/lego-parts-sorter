<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Command;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Infrastructure\Configuration\LegoSorterConfiguration;
use LVC\LegoSorter\Infrastructure\Factory;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Yaml\Yaml;

trait ConfigurationOptionParser
{
    private static function getConfigurationOption(): string
    {
        return 'config';
    }

    private static function getDefaultConfigurationFilePath(): string
    {
        return implode(DIRECTORY_SEPARATOR, [__DIR__, '..', '..', '..', 'var', 'configuration', 'configuration.yaml']);
    }

    private function addConfigurationOption(): void
    {
        $this->addOption(
            self::getConfigurationOption(),
            'c',
            InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
            'Paths to the configuration files.',
            [
                self::getDefaultConfigurationFilePath(),
            ]
        );
    }

    /**
     * @return array{0: ?Catalog, 1: StorageSpace, 2: Matcher}
     */
    private static function parseConfigurationOption(InputInterface $input): array
    {
        $configFilesPath = $input->getOption(self::getConfigurationOption());

        return self::parseConfigurationFilePath($configFilesPath);
    }

    /**
     * @param mixed $configFilePaths
     *
     * @return array{0: ?Catalog, 1: StorageSpace, 2: Matcher}
     */
    private static function parseConfigurationFilePath($configFilePaths): array
    {
        if (!is_array($configFilePaths) || empty($configFilePaths)) {
            throw new \RuntimeException('At least one config file must be given.');
        }

        $filesContent = [];
        foreach ($configFilePaths as $configFilePath) {
            $filesContent[] = Yaml::parseFile($configFilePath);
        }

        $processor = new Processor();

        $configuration = new LegoSorterConfiguration();
        /** @var array<mixed> $processedConfiguration */
        $processedConfiguration = $processor->processConfiguration($configuration, $filesContent);

        return Factory::fromConfiguration($processedConfiguration);
    }
}
