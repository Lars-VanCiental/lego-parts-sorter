<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Printer;

class TemplateEngine
{
    /** @var array<string, string> */
    private array $templatesCache = [];
    private string $templatesDirectory;
    private string $templateFileExtension;

    public function __construct(string $templatesDirectory, string $templateFileExtension)
    {
        $this->templatesDirectory = $templatesDirectory;
        $this->templateFileExtension = $templateFileExtension;
    }

    /**
     * @param array<string, string> $replacements
     */
    public function load(string $templateName, array $replacements): string
    {
        $templateContent = $this->getTemplate($templateName);
        $replacementKeys = array_keys($replacements);
        self::checkReplacements($templateContent, $replacementKeys);

        return str_replace(
            array_map(
                fn (string $key): string => '{{'.$key.'}}',
                $replacementKeys
            ),
            $replacements,
            $templateContent
        );
    }

    private function getTemplate(string $templateName): string
    {
        if (!array_key_exists($templateName, $this->templatesCache)) {
            $templateContent = file_get_contents($this->templatesDirectory.DIRECTORY_SEPARATOR.$templateName.$this->templateFileExtension);
            if ($templateContent === false) {
                throw new \InvalidArgumentException('Could not find template "'.$templateName.'".');
            }

            $this->templatesCache[$templateName] = $templateContent;
        }

        return $this->templatesCache[$templateName];
    }

    /**
     * @param array<int, string> $replacementKeys
     */
    private static function checkReplacements(string $templateContent, array $replacementKeys): void
    {
        $patterns = self::extractPatterns($templateContent);

        if (count($missingPatterns = array_diff($patterns, $replacementKeys)) > 0) {
            throw new \InvalidArgumentException('Missing replacement keys: '.implode(', ', $missingPatterns));
        }

        if (count($uselessPatterns = array_diff($replacementKeys, $patterns)) > 0) {
            throw new \InvalidArgumentException('Too many replacement keys: '.implode(', ', $uselessPatterns));
        }
    }

    /**
     * @return array<int, string>
     */
    private static function extractPatterns(string $templateContent): array
    {
        $matches = [];
        preg_match_all('#{{(?<pattern>[A-Z_]+)}}#', $templateContent, $matches, PREG_UNMATCHED_AS_NULL);

        return $matches['pattern'];
    }
}
