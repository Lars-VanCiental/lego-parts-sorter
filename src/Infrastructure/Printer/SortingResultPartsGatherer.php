<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Printer;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\SortingResult;
use LVC\LegoSorter\Infrastructure\Lego\Illustrated;

trait SortingResultPartsGatherer
{
    /**
     * @return array<string, array<int, Part>>
     */
    private static function gatherPartsByStorageName(SortingResult $sortingResult): array
    {
        /** @var array<string, array<int, Part>> $plan */
        $plan = [];

        foreach ($sortingResult->getStoringPlans() as $storingPlan) {
            $plan[$storingPlan->getStorage()->getName()][] = $storingPlan->getPart();
        }

        ksort($plan);

        return $plan;
    }

    /**
     * @return array<string, array{string, string, string, string, int, ?string}>
     */
    private static function convertPartsToTableRows(Part ...$parts): array
    {
        $partsRows = [];

        foreach ($parts as $part) {
            $partIdentifier = implode(
                '#',
                [
                    $part->getPartNumber(),
                    $part->getType(),
                    $part->getCategory()->getName(),
                    $part->getColor()->getName(),
                ]
            );

            if (!array_key_exists($partIdentifier, $partsRows)) {
                $partsRows[$partIdentifier] = [
                    $part->getPartNumber(),
                    $part->getType(),
                    $part->getColor()->getName(),
                    $part->getCategory()->getName(),
                    0,
                    ($part instanceof Illustrated) ? $part->getImageUrl() : null,
                ];
            }

            $partsRows[$partIdentifier][4]++;
        }

        return $partsRows;
    }
}
