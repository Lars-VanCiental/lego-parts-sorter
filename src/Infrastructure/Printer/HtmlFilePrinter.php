<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Printer;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\SortingResult;
use LVC\LegoSorter\Domain\Storage;

class HtmlFilePrinter implements Printer
{
    use SortingResultPartsGatherer;

    private ?string $fileName;
    private string $outputPath;
    private TemplateEngine $templateEngine;

    public function __construct(string $outputPath, TemplateEngine $templateEngine, ?string $fileName)
    {
        if (!is_dir($outputPath)) {
            throw new \RuntimeException('Invalid directory given to save html file.');
        }
        $this->fileName = $fileName;
        $this->outputPath = $outputPath;
        $this->templateEngine = $templateEngine;
    }

    public function printSortingResult(SortingResult $sortingResult): void
    {
        $filePath = $this->outputPath.DIRECTORY_SEPARATOR.($this->fileName !== null ? $this->fileName : time()).'.html';
        $fileHandler = fopen($filePath, $this->fileName !== null ? 'w' : 'x');
        if ($fileHandler === false) {
            throw new \InvalidArgumentException('Could not create and open file "'.$filePath.'" to save html sorting result report.');
        }

        $date = new \DateTime();
        $sections = [];
        $storages = self::gatherStorages($sortingResult);
        $plan = self::gatherPartsByStorageName($sortingResult);
        foreach ($plan as $storageName => $parts) {
            $sections[] = $this->templateSection(
                'Parts to store in '.$storageName,
                $storages[$storageName]->getDescription(),
                ...$parts
            );
        }
        $unsortedParts = $sortingResult->getUnsortedPartsStack()->getParts();
        if (count($unsortedParts) > 0) {
            $sections[] = $this->templateSection('Unsorted parts', '', ...$unsortedParts);
        }
        $ignoredParts = $sortingResult->getIgnoredPartsList()->getParts();
        if (count($ignoredParts) > 0) {
            $sections[] = $this->templateSection('Ignored parts', '', ...$ignoredParts);
        }

        fwrite(
            $fileHandler,
            $this->templateEngine->load(
                'report',
                [
                    'DATE' => $date->format('d/m/Y G\hi'),
                    'SECTIONS' => implode(PHP_EOL, $sections),
                ]
            )
        );

        fclose($fileHandler);
    }

    private function templateSection(
        string $storageName,
        string $storageDescription,
        Part ...$parts
    ): string {
        return $this->templateEngine->load(
            'section',
            [
                'STORAGE_NAME' => $storageName,
                'STORAGE_DESCRIPTION' => $storageDescription,
                'PARTS' => $this->templateParts(...$parts),
            ]
        );
    }

    private function templateParts(Part ...$part): string
    {
        return implode(
            PHP_EOL,
            array_map(
                fn (array $tableRow): string => $this->templatePart(...$tableRow),
                self::convertPartsToTableRows(...$part)
            )
        );
    }

    private function templatePart(
        string $partNumber,
        string $partType,
        string $partColor,
        string $partCategory,
        int $quantity,
        ?string $imageSource,
    ): string {
        return $this->templateEngine->load(
            'part',
            [
                'PART_TYPE' => $partType,
                'PART_NUMBER' => $partNumber,
                'PART_COLOR' => $partColor,
                'PART_CATEGORY' => $partCategory,
                'IMAGE_SRC' => (string) $imageSource,
                'QUANTITY' => (string) $quantity,
            ]
        );
    }

    /**
     * @return array<string, Storage>
     */
    private static function gatherStorages(SortingResult $sortingResult): array
    {
        $storages = [];

        foreach ($sortingResult->getStoringPlans() as $storingPlan) {
            $storages[$storingPlan->getStorage()->getName()] = $storingPlan->getStorage();
        }

        return $storages;
    }
}
