<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Printer;

use LVC\LegoSorter\Domain\SortingResult;

interface Printer
{
    public function printSortingResult(SortingResult $sortingResult): void;
}
