<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Printer;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\SortingResult;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConsolePrinter implements Printer
{
    use SortingResultPartsGatherer;

    private SymfonyStyle $io;

    public function __construct(SymfonyStyle $io)
    {
        $this->io = $io;
    }

    public function printSortingResult(SortingResult $sortingResult): void
    {
        $plan = self::gatherPartsByStorageName($sortingResult);
        $unsortedParts = $sortingResult->getUnsortedPartsStack()->getParts();
        $ignoredParts = $sortingResult->getIgnoredPartsList()->getParts();

        $this->io->progressStart(count($plan) + (count($unsortedParts) > 0 ? 1 : 0));

        foreach ($plan as $storageName => $parts) {
            $this->io->section($storageName);
            $this->printParts(...$parts);
            $this->io->progressAdvance();

            if (array_key_last($plan) !== $storageName) {
                $proceed = $this->io->confirm('Proceed to next storage? (exit otherwise)');
                if ($proceed !== true) {
                    $this->io->progressFinish();

                    return;
                }
            }
        }

        if (count($unsortedParts) > 0) {
            $this->io->warning('There are '.count($unsortedParts).' unsorted parts.');
            $this->printParts(...$unsortedParts);
            $this->io->progressAdvance();
        }

        if (count($ignoredParts) > 0) {
            $this->io->warning('There are '.count($ignoredParts).' ignored parts.');
            $this->printParts(...$ignoredParts);
            $this->io->progressAdvance();
        }

        $this->io->progressFinish();
    }

    private function printParts(Part ...$part): void
    {
        $this->io->table(
            ['Part number', 'Part type', 'Part color', 'Part category', 'Quantity'],
            self::convertPartsToTableRows(...$part)
        );
    }
}
