<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration;

use LVC\LegoSorter\Infrastructure\Configuration\Provider\RebrickableGlobalConfigurationSection;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class LegoSorterConfiguration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('lego_sorter');

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->append(CatalogConfigurationSection::getNode())
            ->append(StorageSpaceConfigurationSection::getNode())
            ->append(IgnoredPartsConfigurationSection::getNode())
            ->append(RebrickableGlobalConfigurationSection::getNode())
        ;

        return $treeBuilder;
    }
}
