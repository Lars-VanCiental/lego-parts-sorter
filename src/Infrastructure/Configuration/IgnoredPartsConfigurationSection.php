<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;

class IgnoredPartsConfigurationSection
{
    public static function getNode(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('ignored_parts');

        $node->info('Optional configuration to ignored parts when sorting.');
        $node->append(self::getIgnoringMatcherNodeDefinition());

        return $node;
    }

    private static function getIgnoringMatcherNodeDefinition(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('matcher');

        $node->info('Matching parts will be ignored by the sorter.');
        $node->isRequired();
        MatcherNodeConfiguration::appendMatcherNodeDefinitionsWithValidation($node);

        return $node;
    }
}
