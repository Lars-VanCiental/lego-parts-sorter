<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration\Provider;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class CSVConfigurationSection
{
    public static function getNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('csv');

        /** @var ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode->info('Use csv files to describe catalog sets or storage spaces in a directory.');
        $rootNode->children()
            ->append(self::getDirectoryPathNodeDefinition())
        ;

        return $rootNode;
    }

    private static function getDirectoryPathNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('path');

        $node->info('Directory path where are located the files describing sets or storages.');
        $node->isRequired();
        $node->cannotBeEmpty();
        $node->beforeNormalization()
            ->always(
                function (string $value): string {
                    if (substr($value, -1) !== DIRECTORY_SEPARATOR) {
                        $value .= DIRECTORY_SEPARATOR;
                    }

                    return $value;
                }
            )
        ;
        $node->validate()
            ->ifTrue(fn (string $value): bool => !file_exists($value) || !is_dir($value))
            ->thenInvalid('Given directory path for the csv provider does not exist.')
        ;

        return $node;
    }
}
