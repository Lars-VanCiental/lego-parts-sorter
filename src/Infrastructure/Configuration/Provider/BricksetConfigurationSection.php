<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration\Provider;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;

class BricksetConfigurationSection
{
    public static function getNode(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('brickset');

        $node->info('Use the brickset API to get catalog information.');
        $node->children()
            ->append(self::getCredentialNodeDefinition())
        ;

        return $node;
    }

    private static function getCredentialNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('credential');

        $node->info('The required API key can be requested on brickset: https://brickset.com/tools/webservices/requestkey.');
        $node->isRequired();
        $node->cannotBeEmpty();

        return $node;
    }
}
