<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration\Provider;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\FloatNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;

class RebrickableGlobalConfigurationSection
{
    public static function getNode(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('rebrickable');

        $node->info('Use the rebrickable API to get catalog sets or storage information.');
        $node->children()
            ->append(self::getBaseUriNodeDefinition())
            ->append(self::getCredentialNodeDefinition())
            ->append(self::getTimeoutNodeDefinition())
            ->append(self::getCacheNodeDefinition())
            ->append(self::getUsernameNodeDefinition())
            ->append(self::getPasswordNodeDefinition())
        ;

        return $node;
    }

    private static function getBaseUriNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('base_uri');

        $node->info('The required base uri to call the rebrickable API.');
        $node->cannotBeEmpty();
        $node->defaultValue('https://rebrickable.com');

        return $node;
    }

    private static function getCredentialNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('credential');

        $node->info('The required API key can be generated in your rebrickable account.');
        $node->isRequired();
        $node->cannotBeEmpty();

        return $node;
    }

    private static function getTimeoutNodeDefinition(): NodeDefinition
    {
        $node = new FloatNodeDefinition('timeout');

        $node->info('The timeout configuration used when calling the rebrickable API.');
        $node->defaultValue(10);

        return $node;
    }

    private static function getCacheNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('cache');

        $node->info('The directory cache used to store calls to the rebrickable API.');
        $node->defaultValue(
            implode(
                DIRECTORY_SEPARATOR,
                [
                    __DIR__,
                    '..',
                    '..',
                    '..',
                    '..',
                    'var',
                    'cache',
                    'rebrickable',
                ]
            )
        );

        return $node;
    }

    private static function getUsernameNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('username');

        $node->info('The optional username to call the rebrickable API when used as storage.');
        $node->cannotBeEmpty();

        return $node;
    }

    private static function getPasswordNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('password');

        $node->info('The optional password to call the rebrickable API when used as storage.');
        $node->cannotBeEmpty();

        return $node;
    }
}
