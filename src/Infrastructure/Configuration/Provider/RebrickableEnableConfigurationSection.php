<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration\Provider;

use Symfony\Component\Config\Definition\Builder\BooleanNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;

class RebrickableEnableConfigurationSection
{
    public static function getNode(): NodeDefinition
    {
        $node = new BooleanNodeDefinition('rebrickable');

        $node->info('Use the rebrickable API to get catalog or storage information.');

        return $node;
    }
}
