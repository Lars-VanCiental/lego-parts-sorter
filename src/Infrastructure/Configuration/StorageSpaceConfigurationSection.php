<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration;

use LVC\LegoSorter\Infrastructure\Configuration\Provider\CSVConfigurationSection;
use LVC\LegoSorter\Infrastructure\Configuration\Provider\RebrickableEnableConfigurationSection;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;

class StorageSpaceConfigurationSection
{
    public static function getNode(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('storages');

        $node->info('Configuration of your storage space.');
        $node->isRequired();
        $node->children()
            ->append(self::getProviderNodeDefinition())
            ->append(self::getMatchingRulesNodeDefinition())
        ;

        return $node;
    }

    private static function getProviderNodeDefinition(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('provider');

        $node->isRequired();
        $node->children()
            ->append(CSVConfigurationSection::getNode())
            ->append(RebrickableEnableConfigurationSection::getNode())
        ;
        $node->validate()
            ->ifTrue(fn (array $value): bool => count(array_keys($value)) > 1)
            ->thenInvalid('There can be only one provider configured for your storages.')
        ;
        $node->validate()
            ->ifTrue(fn (array $value): bool => count(array_keys($value)) < 1)
            ->thenInvalid('There must be one provider configured for your storages.')
        ;

        return $node;
    }

    private static function getMatchingRulesNodeDefinition(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('matching_rules');

        $node->isRequired();
        $node->cannotBeEmpty();
        $node->arrayPrototype()->children()
            ->append(self::getStorageNameNodeDefinition())
            ->append(self::getStorageDescriptionNodeDefinition())
            ->append(self::getStorageMatcherNodeDefinition())
        ;
        $node->info('A list of your storages with rules to put Lego parts inside it.');

        return $node;
    }

    private static function getStorageNameNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('name');

        $node->info('Name of the storage, as given by your provider.');
        $node->isRequired();
        $node->cannotBeEmpty();
        $node->example('"Red box"');

        return $node;
    }

    private static function getStorageDescriptionNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('description');

        $node->info('Optional description of the storage, to help during the sorting process.');
        $node->example('"Box containing red pieces"');
        $node->defaultValue('');

        return $node;
    }

    private static function getStorageMatcherNodeDefinition(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('matcher');

        $node->isRequired();
        MatcherNodeConfiguration::appendMatcherNodeDefinitionsWithValidation($node);

        return $node;
    }
}
