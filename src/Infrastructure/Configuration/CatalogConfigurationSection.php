<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration;

use LVC\LegoSorter\Infrastructure\Configuration\Provider\BricksetConfigurationSection;
use LVC\LegoSorter\Infrastructure\Configuration\Provider\CSVConfigurationSection;
use LVC\LegoSorter\Infrastructure\Configuration\Provider\RebrickableEnableConfigurationSection;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;

class CatalogConfigurationSection
{
    public static function getNode(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('catalog');

        $node->info('Configuration on how to retrieve sets information.');
        $node->isRequired();
        $node->children()
            ->append(self::getProviderNodeDefinition())
        ;

        return $node;
    }

    private static function getProviderNodeDefinition(): NodeDefinition
    {
        $node = new ArrayNodeDefinition('provider');

        $node->isRequired();
        $node->children()
            ->append(BricksetConfigurationSection::getNode())
            ->append(CSVConfigurationSection::getNode())
            ->append(RebrickableEnableConfigurationSection::getNode())
        ;
        $node->validate()
            ->ifTrue(fn (array $value): bool => count(array_keys($value)) > 1)
            ->thenInvalid('There can be only one provider configured for the catalog.')
        ;
        $node->validate()
            ->ifTrue(fn (array $value): bool => count(array_keys($value)) < 1)
            ->thenInvalid('There must be one provider configured for the catalog.')
        ;

        return $node;
    }
}
