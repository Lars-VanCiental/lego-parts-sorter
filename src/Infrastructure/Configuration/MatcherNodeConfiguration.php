<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Configuration;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\ScalarNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\Builder\VariableNodeDefinition;

class MatcherNodeConfiguration
{
    public static function appendMatcherNodeDefinitionsWithValidation(ArrayNodeDefinition $parentBuilder): void
    {
        $parentBuilder->children()
            ->append(self::getMatcherRuleNodeDefinition())
            ->append(self::getMatcherValueNodeDefinition())
            ->append(self::getMatcherMatchersNodeDefinition())
        ;
        $parentBuilder->validate()
            ->ifTrue(fn (array $value): bool => in_array($value['rule'], ['default', 'null']) && count($value) > 1)
            ->thenInvalid('Matchers "default" and "null" support no other configuration than "rule".')
        ;
        $parentBuilder->validate()
            ->ifTrue(fn (array $value): bool => in_array($value['rule'], ['category', 'color', 'type']) && !array_key_exists('value', $value))
            ->thenInvalid('Matchers "category", "color" and "type" need a "value" configuration.')
        ;
        $parentBuilder->validate()
            ->ifTrue(fn (array $value): bool => in_array($value['rule'], ['category', 'color', 'type']) && count($value) > 2)
            ->thenInvalid('Matchers "category", "color" and "type" support no other configuration than "rule" and "value".')
        ;
        $parentBuilder->validate()
            ->ifTrue(fn (array $value): bool => in_array($value['rule'], ['and', 'or']) && !array_key_exists('matchers', $value))
            ->thenInvalid('Matchers "and" and "or" need a "matchers".')
        ;
        $parentBuilder->validate()
            ->ifTrue(fn (array $value): bool => in_array($value['rule'], ['and', 'or']) && count($value) > 2)
            ->thenInvalid('Matchers "and" and "or" support no other configuration than "rule" and "matchers".')
        ;
    }

    private static function getMatcherRuleNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('rule');

        $node->info('Rule to be used by the matcher.');
        $node->example('One of [and,category,color,default,null,number,or,type].');
        $node->isRequired();
        $node->cannotBeEmpty();
        $node->validate()
            ->ifNotInArray(
                [
                    'and',
                    'category',
                    'color',
                    'default',
                    'null',
                    'number',
                    'or',
                    'type',
                ]
            )
            ->thenInvalid('Invalid rule "%s" given for matcher.')
        ;

        return $node;
    }

    private static function getMatcherValueNodeDefinition(): NodeDefinition
    {
        $node = new ScalarNodeDefinition('value');

        $node->info('Value to match for rules "category", "color", and "type".');
        $node->cannotBeEmpty();

        return $node;
    }

    private static function getMatcherMatchersNodeDefinition(): NodeDefinition
    {
        $node = new VariableNodeDefinition('matchers');

        $node->info('Operand matchers to be used by rules "and" and "or".');
        $node->example('matchers: [{rule: "color", value: "orange"}, {rule: "type", value: "transportation"]');
        $node->cannotBeEmpty();
        $node->beforeNormalization()
            ->ifTrue(fn ($value): bool => !is_array($value))
            ->thenInvalid('Configuration "matchers" must be an array of valid matcher\'s configurations.')
        ;
        $node->beforeNormalization()
            ->always(
                function (array $value) {
                    $configs = [];

                    foreach ($value as $name => $config) {
                        $treeBuilder = new TreeBuilder((string) $name);
                        /** @var ArrayNodeDefinition $rootNode */
                        $rootNode = $treeBuilder->getRootNode();
                        self::appendMatcherNodeDefinitionsWithValidation($rootNode);
                        $configs[$name] = $treeBuilder->buildTree()->finalize($config);
                    }

                    return $configs;
                }
            )
        ;

        return $node;
    }
}
