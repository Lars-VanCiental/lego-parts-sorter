<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Lego;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Lego\Set;

class SetValueObject implements Set
{
    /** @var Part[] */
    private array $parts;

    public function __construct(
        private string $setNumber,
        private string $name,
        Part ...$parts
    ) {
        $this->parts = $parts;
    }

    public function getSetNumber(): string
    {
        return $this->setNumber;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getParts(): array
    {
        return $this->parts;
    }
}
