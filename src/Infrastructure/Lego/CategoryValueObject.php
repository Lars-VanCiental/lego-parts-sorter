<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Lego;

use LVC\LegoSorter\Domain\Lego\Category;

class CategoryValueObject implements Category
{
    public function __construct(
        private string $id,
        private string $name,
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
