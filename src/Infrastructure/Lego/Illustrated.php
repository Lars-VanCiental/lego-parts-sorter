<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Lego;

interface Illustrated
{
    public function getImageUrl(): ?string;
}
