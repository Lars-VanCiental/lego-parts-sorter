<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Lego;

use LVC\LegoSorter\Domain\Lego\Category;
use LVC\LegoSorter\Domain\Lego\Color;
use LVC\LegoSorter\Domain\Lego\Part;

class PartValueObject implements Part, Illustrated
{
    public function __construct(
        private string $partNumber,
        private string $type,
        private Color $color,
        private Category $category,
        private ?string $imageUrl,
    ) {
    }

    public function getPartNumber(): string
    {
        return $this->partNumber;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getColor(): Color
    {
        return $this->color;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function isSame(Part $part): bool
    {
        return $this->getPartNumber() === $part->getPartNumber()
            && $this->getType() === $part->getType()
            && $this->getCategory()->getName() === $part->getCategory()->getName()
            && $this->getColor()->getName() === $part->getColor()->getName()
            ;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }
}
