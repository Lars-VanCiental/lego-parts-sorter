<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Storage;
use LVC\LegoSorter\Domain\UnsortedPartsStack;

class RebrickableStorage implements Storage
{
    private ?int $id;
    private string $name;
    private string $description;
    private Matcher $matcher;
    /** @var array<Part> */
    private array $parts;
    /** @var array<string, array{part: Part, default: int, changes: int}> */
    private array $partsTracking = [];

    public function __construct(
        ?int $id,
        string $name,
        string $description,
        Matcher $matcher,
        Part ...$parts
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->matcher = $matcher;
        $this->parts = $parts;

        $this->resetTracking();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getMatcher(): Matcher
    {
        return $this->matcher;
    }

    public function empty(): UnsortedPartsStack
    {
        $unsortedPartsStack = new UnsortedPartsStack();

        foreach ($this->parts as $part) {
            $unsortedPartsStack->stackPart($part);

            $partKey = self::getPartKey($part);
            if (!array_key_exists($partKey, $this->partsTracking)) {
                throw new \InvalidArgumentException('Cannot keep track of the removal of a part that is not already tracked in the storage.');
            }
            $this->partsTracking[$partKey]['changes']--;
        }

        $this->parts = [];

        return $unsortedPartsStack;
    }

    public function storePart(Part $part): void
    {
        $this->parts[] = $part;

        $partKey = self::getPartKey($part);
        if (!array_key_exists($partKey, $this->partsTracking)) {
            $this->partsTracking[$partKey] = ['part' => $part, 'default' => 0, 'changes' => 0];
        }
        $this->partsTracking[$partKey]['changes']++;
    }

    public function getParts(): array
    {
        return $this->parts;
    }

    public function create(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array{added: array<int, array{Part, int}>, removed: array<int, Part>, updated: array<int, array{Part, int}>, emptied: bool}
     */
    public function computeChanges(): array
    {
        $outcome = ['added' => [], 'removed' => [], 'updated' => [], 'emptied' => false];

        foreach ($this->partsTracking as $partTracking) {
            if ($partTracking['default'] + $partTracking['changes'] < 0) {
                throw new \InvalidArgumentException('Tracked part in a storage has a negative outcome.');
            }

            if ($partTracking['default'] === 0) {
                $outcome['added'][] = [$partTracking['part'], $partTracking['changes']];

                continue;
            }

            if ($partTracking['default'] + $partTracking['changes'] === 0) {
                $outcome['removed'][] = $partTracking['part'];

                continue;
            }

            if ($partTracking['changes'] !== 0) {
                $outcome['updated'][] = [$partTracking['part'], $partTracking['default'] + $partTracking['changes']];

                continue;
            }
        }

        $outcome['emptied'] = (count($this->partsTracking) > 0) && (count($this->partsTracking) === count($outcome['removed']));

        return $outcome;
    }

    public function resetTracking(): void
    {
        $this->partsTracking = [];

        foreach ($this->parts as $part) {
            $partKey = self::getPartKey($part);
            if (!array_key_exists($partKey, $this->partsTracking)) {
                $this->partsTracking[$partKey] = ['part' => $part, 'default' => 0, 'changes' => 0];
            }
            $this->partsTracking[$partKey]['default']++;
        }
    }

    private static function getPartKey(Part $part): string
    {
        return $part->getPartNumber().'#'.$part->getColor()->getId();
    }
}
