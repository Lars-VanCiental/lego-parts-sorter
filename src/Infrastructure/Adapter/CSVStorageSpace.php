<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Application\WritableStorageSpace;
use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Storage;
use LVC\LegoSorter\Infrastructure\CSV\DirectoryHandler;
use LVC\LegoSorter\Infrastructure\CSV\FileHandler;
use LVC\LegoSorter\Infrastructure\Lego\CategoryValueObject;
use LVC\LegoSorter\Infrastructure\Lego\ColorValueObject;
use LVC\LegoSorter\Infrastructure\Lego\PartValueObject;

class CSVStorageSpace implements StorageSpace, WritableStorageSpace
{
    private string $directoryPath;
    /** @var array<string, array{string, Matcher}> */
    private array $storagesConfigurations;

    /**
     * @param array<string, array{string, Matcher}> $storagesConfigurations
     */
    public function __construct(
        string $directoryPath,
        array $storagesConfigurations,
    ) {
        $this->directoryPath = $directoryPath;
        $this->storagesConfigurations = $storagesConfigurations;
    }

    public function getDirectoryPath(): string
    {
        return $this->directoryPath;
    }

    public function takeOutStorages(): array
    {
        $storages = [];

        $storagesParts = $this->readStorageFiles();

        foreach ($this->storagesConfigurations as $storageName => [$description, $matcher]) {
            if (!array_key_exists($storageName, $storagesParts)) {
                $this->writeStorageFile($storageName);
            }

            $storages[] = $this->createStorage(
                $storageName,
                $description,
                $matcher,
                ...$storagesParts[$storageName] ?? []
            );
        }

        return $storages;
    }

    public function putAwayStorages(Storage ...$storages): void
    {
        foreach ($storages as $storage) {
            $this->writeStorageFile($storage->getName(), ...$storage->getParts());
        }
    }

    /**
     * @return array<string, Part[]>
     */
    private function readStorageFiles(): array
    {
        $directory = new DirectoryHandler($this->directoryPath);

        $storageNames = [];

        /** @var FileHandler $storageFile */
        foreach ($directory->getFiles() as $storageFile) {
            $headerSkipped = false;

            foreach ($storageFile->read() as $data) {
                if (count($data) !== 4) {
                    throw new \RuntimeException('Corrupted csv file "'.$storageFile->getFilePath().'", each line must contains 4 value.');
                }
                if ($headerSkipped === false) {
                    $headerSkipped = true;
                    continue;
                }

                [$partNumber, $type, $color, $category] = $data;
                $storageNames[$storageFile->getFileName()][] = new PartValueObject(
                    $partNumber,
                    $type,
                    new ColorValueObject($color, $color),
                    new CategoryValueObject($category, $category),
                    null
                );
            }
        }

        return $storageNames;
    }

    private function writeStorageFile(string $storageName, Part ...$parts): void
    {
        $file = new FileHandler($this->directoryPath.$storageName.'.csv');

        $file->write(self::createGeneratorFromParts(...$parts));
    }

    /**
     * @return \Generator<array{string, string, string}>
     */
    private static function createGeneratorFromParts(Part ...$parts): \Generator
    {
        yield ['PartNumber', 'Type', 'Color', 'Category'];

        foreach ($parts as $part) {
            yield [
                $part->getPartNumber(),
                $part->getType(),
                $part->getColor()->getName(),
                $part->getCategory()->getName(),
            ];
        }
    }

    public function createStorage(
        string $storageName,
        string $storageDescription,
        Matcher $matcher,
        Part ...$storedParts
    ): Storage {
        return new CSVStorage($storageName, $storageDescription, $matcher, ...$storedParts);
    }
}
