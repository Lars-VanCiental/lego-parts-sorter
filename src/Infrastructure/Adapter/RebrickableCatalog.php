<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Domain\Lego\Set;
use LVC\LegoSorter\Infrastructure\Clients\Rebrickable;
use LVC\LegoSorter\Infrastructure\Lego\ColorValueObject;
use LVC\LegoSorter\Infrastructure\Lego\SetValueObject;

class RebrickableCatalog implements Catalog
{
    use Rebrickable\PaginationHandler;
    use Rebrickable\PartsGatherer;

    private Rebrickable\CatalogueClient $catalogueClient;

    public function __construct(
        Rebrickable\CatalogueClient $client
    ) {
        $this->catalogueClient = $client;
    }

    public function getColors(): array
    {
        $colors = [];

        foreach (self::goThroughPagination(fn (int $page): array => $this->catalogueClient->getColors($page)) as $colorData) {
            $colors[] = new ColorValueObject($colorData['id'], $colorData['name']);
        }

        return $colors;
    }

    public function getSet(string $setNumber): ?Set
    {
        if (!preg_match('#-\d$#', $setNumber)) {
            $setNumber .= '-1';
        }

        $setData = $this->catalogueClient->getSet($setNumber);

        return new SetValueObject(
            $setData['set_num'],
            $setData['name'],
            ...$this->gatherParts(
                fn (int $page): array => $this->catalogueClient->getSetParts($setNumber, $page),
            ),
        );
    }
}
