<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Storage;
use LVC\LegoSorter\Domain\UnsortedPartsStack;

class CSVStorage implements Storage
{
    private string $name;
    private string $description;
    private Matcher $matcher;
    /** @var Part[] */
    private array $storedParts;

    public function __construct(
        string $name,
        string $description,
        Matcher $matcher,
        Part ...$storedParts
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->matcher = $matcher;
        $this->storedParts = $storedParts;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getMatcher(): Matcher
    {
        return $this->matcher;
    }

    public function empty(): UnsortedPartsStack
    {
        $unsortedPartsStack = new UnsortedPartsStack();

        foreach ($this->storedParts as $part) {
            $unsortedPartsStack->stackPart($part);
        }

        $this->storedParts = [];

        return $unsortedPartsStack;
    }

    public function storePart(Part $part): void
    {
        $this->storedParts[] = $part;
    }

    public function getParts(): array
    {
        return $this->storedParts;
    }
}
