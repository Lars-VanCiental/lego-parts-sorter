<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Domain\Lego\Set;
use LVC\LegoSorter\Infrastructure\CSV\DirectoryHandler;
use LVC\LegoSorter\Infrastructure\CSV\FileHandler;
use LVC\LegoSorter\Infrastructure\Lego\CategoryValueObject;
use LVC\LegoSorter\Infrastructure\Lego\ColorValueObject;
use LVC\LegoSorter\Infrastructure\Lego\SetValueObject;

class CSVCatalog implements Catalog
{
    use CSVSetMaker;

    private string $directoryPath;

    public function __construct(
        string $directoryPath,
    ) {
        $this->directoryPath = $directoryPath;
    }

    public function getDirectoryPath(): string
    {
        return $this->directoryPath;
    }

    public function getCategories(): array
    {
        $categories = [];

        $directory = new DirectoryHandler($this->directoryPath);

        /* @var FileHandler $file */
        foreach ($directory->getFiles() as $setFile) {
            $headerSkipped = false;
            foreach ($setFile->read() as $data) {
                if (count($data) !== 4) {
                    throw new \RuntimeException('Corrupted csv file "'.$setFile->getFilePath().'", each line must contains 4 value.');
                }
                if ($headerSkipped === false) {
                    $headerSkipped = true;
                    continue;
                }

                [, , , $category] = $data;

                if (!in_array($category, $categories)) {
                    $categories[] = new CategoryValueObject($category, $category);
                }
            }
        }

        sort($categories);

        return $categories;
    }

    public function getColors(): array
    {
        $colors = [];

        $directory = new DirectoryHandler($this->directoryPath);

        /* @var FileHandler $file */
        foreach ($directory->getFiles() as $setFile) {
            $headerSkipped = false;
            foreach ($setFile->read() as $data) {
                if (count($data) !== 4) {
                    throw new \RuntimeException('Corrupted csv file "'.$setFile->getFilePath().'", each line must contains 4 value.');
                }
                if ($headerSkipped === false) {
                    $headerSkipped = true;
                    continue;
                }

                [, , $color, ] = $data;

                if (!in_array($color, $colors)) {
                    $colors[] = new ColorValueObject($color, $color);
                }
            }
        }

        sort($colors);

        return $colors;
    }

    public function getSet(string $setNumber): ?Set
    {
        $directory = new DirectoryHandler($this->directoryPath);

        /** @var FileHandler $file */
        foreach ($directory->getFiles() as $file) {
            [$fileSetNumber, $fileSetName] = explode(' - ', $file->getFileName(), 2);

            if ($setNumber !== $fileSetNumber) {
                continue;
            }

            return new SetValueObject(
                $fileSetNumber,
                $fileSetName,
                ...$this->readSetFile($file)
            );
        }

        return null;
    }
}
