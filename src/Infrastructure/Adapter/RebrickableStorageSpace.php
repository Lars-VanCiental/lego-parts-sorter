<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Application\WritableStorageSpace;
use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Storage;
use LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

class RebrickableStorageSpace implements StorageSpace, WritableStorageSpace
{
    use Rebrickable\PaginationHandler;
    use Rebrickable\PartsGatherer;

    private Rebrickable\CatalogueClient $catalogueClient;
    private Rebrickable\StorageClient $storageClient;
    /** @var array<string, array{string, Matcher}> */
    private array $storagesConfigurations;

    /**
     * @param array<string, array{string, Matcher}> $storagesConfigurations
     */
    public function __construct(
        Rebrickable\CatalogueClient $catalogueClient,
        Rebrickable\StorageClient $storageClient,
        array $storagesConfigurations,
    ) {
        $this->catalogueClient = $catalogueClient;
        $this->storageClient = $storageClient;
        $this->storagesConfigurations = $storagesConfigurations;
    }

    public function takeOutStorages(): array
    {
        $clientCallback = function (int $page): array {
            return $this->storageClient->getUserPartLists($page);
        };

        $storages = [];
        foreach (self::goThroughPagination($clientCallback) as $partListData) {
            if (!array_key_exists($partListData['name'], $this->storagesConfigurations)) {
                continue;
            }

            $storages[$partListData['name']] = new RebrickableStorage(
                $partListData['id'],
                $partListData['name'],
                $this->storagesConfigurations[$partListData['name']][0],
                $this->storagesConfigurations[$partListData['name']][1],
                ...$this->gatherParts(
                    fn (int $page): array => $this->storageClient->getUserPartListParts($partListData['id'], $page),
                )
            );
        }

        foreach ($this->storagesConfigurations as $storageName => $storagesConfiguration) {
            if (!array_key_exists($storageName, $storages)) {
                $storages[$storageName] = $this->createStorage(
                    $storageName,
                    $this->storagesConfigurations[$storageName][0],
                    $this->storagesConfigurations[$storageName][1],
                );
            }
        }

        return array_filter(
            array_map(
                fn (string $storageName) => $storages[$storageName] ?? null,
                array_keys($this->storagesConfigurations)
            ),
        );
    }

    public function putAwayStorages(Storage ...$storages): void
    {
        foreach ($storages as $storage) {
            if (!$storage instanceof RebrickableStorage) {
                throw new \InvalidArgumentException('Only rebrickable storage can be stored inside the rebrickable storage space.');
            }
        }

        /** @var RebrickableStorage $storage */
        foreach ($storages as $storage) {
            $storageId = $storage->getId();
            if ($storageId === null) {
                $storage->create($storageId = $this->storageClient->createUserPartList($storage->getName())['id']);
            }

            $changes = $storage->computeChanges();
            if ($changes['emptied'] === true) {
                $this->storageClient->deleteUserPartList($storageId);
                $storage->create(
                    $this->storageClient->createUserPartList($storage->getName())['id']
                );

                continue;
            }

            foreach ($changes['added'] as $addedPartQuantity) {
                $this->storageClient->addUserPartListPart($storageId, $addedPartQuantity[0], $addedPartQuantity[1]);
            }
            foreach ($changes['removed'] as $removedPart) {
                $this->storageClient->removeUserPartListPart($storageId, $removedPart);
            }
            foreach ($changes['updated'] as $updatedPartQuantity) {
                $this->storageClient->updateUserPartListPart($storageId, $updatedPartQuantity[0], $updatedPartQuantity[1]);
            }

            $storage->resetTracking();
        }
    }

    public function createStorage(string $storageName, string $storageDescription, Matcher $matcher, Part ...$storedParts): Storage
    {
        return new RebrickableStorage(
            null,
            $storageName,
            $storageDescription,
            $matcher,
            ...$storedParts
        );
    }
}
