<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Domain\Lego\Category;
use LVC\LegoSorter\Domain\Lego\Color;
use LVC\LegoSorter\Domain\Lego\Set;

class CachedCatalog implements Catalog
{
    /** @var array<string, ?Set> */
    private array $cachedSets = [];
    /** @var array<int, Category> */
    private array $cachedCategories;
    /** @var array<int, Color> */
    private array $cachedColors;
    private Catalog $catalog;

    public function __construct(
        Catalog $catalog
    ) {
        $this->catalog = $catalog;
    }

    public function getCatalog(): Catalog
    {
        return $this->catalog;
    }

    public function getCategories(): array
    {
        if (!isset($this->cachedCategories)) {
            $this->cachedCategories = $this->catalog->getCategories();
        }

        return $this->cachedCategories;
    }

    public function getColors(): array
    {
        if (!isset($this->cachedColors)) {
            $this->cachedColors = $this->catalog->getColors();
        }

        return $this->cachedColors;
    }

    public function getSet(string $setNumber): ?Set
    {
        if (!array_key_exists($setNumber, $this->cachedSets)) {
            $this->cachedSets[$setNumber] = $this->catalog->getSet($setNumber);
        }

        return $this->cachedSets[$setNumber];
    }
}
