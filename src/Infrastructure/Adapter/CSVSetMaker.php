<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Adapter;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Infrastructure\CSV\FileHandler;
use LVC\LegoSorter\Infrastructure\Lego\CategoryValueObject;
use LVC\LegoSorter\Infrastructure\Lego\ColorValueObject;
use LVC\LegoSorter\Infrastructure\Lego\PartValueObject;

trait CSVSetMaker
{
    /**
     * @return array<Part>
     */
    private function readSetFile(FileHandler $setFile): array
    {
        $parts = [];

        $headerSkipped = false;
        foreach ($setFile->read() as $data) {
            if (count($data) !== 4) {
                throw new \RuntimeException('Corrupted csv file "'.$setFile->getFilePath().'", each line must contains 4 value.');
            }
            if ($headerSkipped === false) {
                $headerSkipped = true;
                continue;
            }

            [$partNumber, $type, $color, $category] = $data;
            $parts[] = new PartValueObject(
                $partNumber,
                $type,
                new ColorValueObject($color, $color),
                new CategoryValueObject($category, $category),
                null
            );
        }

        return $parts;
    }
}
