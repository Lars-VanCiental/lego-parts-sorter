<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleRetry\GuzzleRetryMiddleware;
use LVC\LegoSorter\Infrastructure\Http\CachedClient;
use LVC\LegoSorter\Infrastructure\Http\GuzzleWrapperClient;
use Spatie\GuzzleRateLimiterMiddleware\RateLimiterMiddleware;

class ClientFactory
{
    /**
     * @param array<mixed> $configuration
     *
     * @return array{0: CatalogueClient, 1: ?StorageClient}
     */
    public static function buildClients(array $configuration): array
    {
        [$baseUri, $apiKey, $timeout, $username, $password, $cacheDirectoryPath] = self::extractArguments($configuration);

        $guzzleClient = new Client(
            [
                'base_uri' => $baseUri,
                'timeout' => $timeout,
                'verify' => false,
                'handler' => $handlerStack = HandlerStack::create(),
            ]
        );
        $handlerStack->push(RateLimiterMiddleware::perSecond(1));
        $handlerStack->push(GuzzleRetryMiddleware::factory(['retry_on_timeout' => true]));
        $httpClient = new GuzzleWrapperClient($guzzleClient);
        $cachedClient = new CachedClient($cacheDirectoryPath, $httpClient);

        return [
            new CatalogueClient(
                $apiKey,
                $cachedClient,
            ),
            $username !== null && $password !== null ? new StorageClient(
                $apiKey,
                $httpClient,
                $username,
                $password,
            ) : null,
        ];
    }

    /**
     * @param array<mixed> $configuration
     *
     * @return array{0: string, 1: string, 2: float, 3: ?string, 4: ?string, 5: ?string}
     */
    private static function extractArguments(array $configuration): array
    {
        if (!array_key_exists('rebrickable', $configuration)) {
            throw new \InvalidArgumentException('Invalid configuration given for the rebrickable client.');
        }

        $baseUri = $configuration['rebrickable']['base_uri'] ?? null;
        if (!is_string($baseUri) || empty($baseUri)) {
            throw new \InvalidArgumentException('Invalid configuration given for the base uri of the rebrickable client.');
        }

        $credential = $configuration['rebrickable']['credential'] ?? null;
        if (!is_string($credential) || empty($credential)) {
            throw new \InvalidArgumentException('Invalid configuration given for the credential of the rebrickable client.');
        }

        $timeout = $configuration['rebrickable']['timeout'] ?? null;
        if (!is_numeric($timeout)) {
            throw new \InvalidArgumentException('Invalid configuration given for the timeout of the rebrickable client.');
        }

        $username = $configuration['rebrickable']['username'] ?? null;
        if ($username !== null && (!is_string($username) || empty($username))) {
            throw new \InvalidArgumentException('Invalid configuration given for the username of the rebrickable client.');
        }

        $password = $configuration['rebrickable']['password'] ?? null;
        if ($password !== null && (!is_string($password) || empty($password))) {
            throw new \InvalidArgumentException('Invalid configuration given for the password of the rebrickable client.');
        }

        $cacheDirectoryPath = $configuration['rebrickable']['cache'] ?? null;
        if ($cacheDirectoryPath !== null && !is_string($cacheDirectoryPath)) {
            throw new \InvalidArgumentException('Invalid configuration given for the cache of the rebrickable client.');
        }
        if ($cacheDirectoryPath !== null && !is_dir($cacheDirectoryPath)) {
            throw new \InvalidArgumentException('Invalid configuration given for the cache of the rebrickable client, must be an existing directory.');
        }

        return [$baseUri, $credential, (float) $timeout, $username, $password, $cacheDirectoryPath];
    }
}
