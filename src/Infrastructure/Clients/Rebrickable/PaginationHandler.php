<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

trait PaginationHandler
{
    /**
     * @template R
     *
     * @param callable(int): array{next: string, results: array<R>} $clientFunction
     *
     * @return \Generator<R>
     */
    private static function goThroughPagination(callable $clientFunction, int $page = 1): \Generator
    {
        $paginationData = $clientFunction($page);
        yield from $paginationData['results'];

        if (array_key_exists('next', $paginationData) && !empty($paginationData['next'])) {
            yield from self::goThroughPagination($clientFunction, $page + 1);
        }
    }
}
