<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

use LVC\LegoSorter\Domain\Lego\Category;
use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Infrastructure\Lego\CategoryValueObject;
use LVC\LegoSorter\Infrastructure\Lego\ColorValueObject;
use LVC\LegoSorter\Infrastructure\Lego\PartValueObject;

trait PartsGatherer
{
    /**
     * @param callable(int): array{next: string, results: array<array{part: array{part_num: string, name: string, part_cat_id: int, part_img_url: string}, color: array{id: string, name: string}, quantity: int}>} $clientCallback
     *
     * @return Part[]
     */
    private function gatherParts(callable $clientCallback): array
    {
        $parts = [];
        foreach (self::goThroughPagination($clientCallback) as $partData) {
            array_push(
                $parts,
                ...array_pad(
                    [],
                    $partData['quantity'],
                    new PartValueObject(
                        $partData['part']['part_num'],
                        $partData['part']['name'],
                        new ColorValueObject((string) $partData['color']['id'], $partData['color']['name']),
                        $this->searchCategory((string) $partData['part']['part_cat_id']),
                        $partData['part']['part_img_url']
                    ),
                )
            );
        }

        return $parts;
    }

    private function searchCategory(string $categoryId): Category
    {
        foreach ($this->getCategories() as $category) {
            if ($category->getId() === $categoryId) {
                return $category;
            }
        }

        throw new \RuntimeException('Could not find the category "'.$categoryId.'" inside the rebrickable catalog.');
    }

    /**
     * @return array<int, Category>
     */
    public function getCategories(): array
    {
        static $categories = null;

        if ($categories === null) {
            $categories = [];

            foreach (self::goThroughPagination(fn (int $page): array => $this->catalogueClient->getCategories($page)) as $categoryData) {
                $categories[] = new CategoryValueObject((string) $categoryData['id'], $categoryData['name']);
            }
        }

        return $categories;
    }
}
