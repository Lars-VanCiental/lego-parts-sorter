<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

use LVC\LegoSorter\Infrastructure\Http\Client;

class CatalogueClient
{
    use HttpRequestHelper;

    private const PAGE_SIZE = 250;

    private string $apiKey;
    private Client $httpClient;

    public function __construct(
        string $apiKey,
        Client $httpClient,
    ) {
        $this->apiKey = $apiKey;
        $this->httpClient = $httpClient;
    }

    /**
     * @return array{next: string, results: array<array{id: string, name: string}>}
     */
    public function getCategories(int $page): array
    {
        /* @phpstan-ignore-next-line */
        return $this->doRequest('/api/v3/lego/part_categories?page_size='.self::PAGE_SIZE.'&page='.$page.'&ordering=name');
    }

    /**
     * @return array{next: string, results: array<array{id: string, name: string}>}
     */
    public function getColors(int $page): array
    {
        /* @phpstan-ignore-next-line */
        return $this->doRequest('/api/v3/lego/colors?page_size='.self::PAGE_SIZE.'&page='.$page.'&ordering=is_trans,name');
    }

    /**
     * @return array{set_num: string, name: string}
     */
    public function getSet(string $setNumber): array
    {
        /* @phpstan-ignore-next-line */
        return $this->doRequest('/api/v3/lego/sets/'.$setNumber.'/');
    }

    /**
     * @return array{next: string, results: array<array{part: array{part_num: string, name: string, part_cat_id: int, part_img_url: string}, color: array{id: string, name: string}, quantity: int}>}
     */
    public function getSetParts(string $setNumber, int $page): array
    {
        /* @phpstan-ignore-next-line */
        return $this->doRequest('/api/v3/lego/sets/'.$setNumber.'/parts/?page_size='.self::PAGE_SIZE.'&page='.$page.'&inc_part_details=0&inc_minifig_parts=1&inc_color_details=0');
    }
}
