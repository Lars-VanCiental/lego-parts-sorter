<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

use LVC\LegoSorter\Infrastructure\Http\Verb;

trait HttpRequestHelper
{
    /**
     * @param array<mixed> $options
     *
     * @return array<mixed>
     */
    private function doRequest(
        string $endpoint,
        Verb $verb = Verb::GET,
        array $options = [],
    ): array {
        return $this->httpClient->doRequest(
            $endpoint,
            $verb,
            array_merge(
                [
                    'headers' => [
                        'Accept' => 'application/json',
                        'Authorization' => 'key '.$this->apiKey,
                    ],
                ],
                $options,
            ),
        );
    }
}
