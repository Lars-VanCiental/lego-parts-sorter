<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Infrastructure\Http\Client;
use LVC\LegoSorter\Infrastructure\Http\Verb;

class StorageClient
{
    use HttpRequestHelper;

    private const PAGE_SIZE = 50;

    private string $apiKey;
    private Client $httpClient;
    private string $password;
    private string $username;

    public function __construct(
        string $apiKey,
        Client $httpClient,
        string $username,
        string $password,
    ) {
        $this->apiKey = $apiKey;
        $this->httpClient = $httpClient;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return array{next: string, results: array<array{id: int, name: string}>}
     */
    public function getUserPartLists(int $page): array
    {
        /* @phpstan-ignore-next-line */
        return $this->doRequest(
            '/api/v3/users/'.$this->getUserToken().'/partlists/?page_size='.self::PAGE_SIZE.'&page='.$page,
        );
    }

    /**
     * @return array{id: int, name: string}
     */
    public function createUserPartList(string $name): array
    {
        /* @phpstan-ignore-next-line */
        return $this->doRequest(
            '/api/v3/users/'.$this->getUserToken().'/partlists/',
            Verb::POST,
            [
                'form_params' => [
                    'name' => $name,
                ],
            ],
        );
    }

    /**
     * @return array{}
     */
    public function deleteUserPartList(int $partListId): array
    {
        return $this->doRequest(
            '/api/v3/users/'.$this->getUserToken().'/partlists/'.$partListId.'/',
            Verb::DELETE,
        );
    }

    /**
     * @return array{next: string, results: array<array{part: array{part_num: string, name: string, part_cat_id: int, part_img_url: string}, color: array{id: string, name: string}, quantity: int}>}
     */
    public function getUserPartListParts(int $partListId, int $page): array
    {
        /* @phpstan-ignore-next-line */
        return $this->doRequest(
            '/api/v3/users/'.$this->getUserToken().'/partlists/'.$partListId.'/parts/?page_size='.self::PAGE_SIZE.'&page='.$page.'&inc_part_details=0&inc_color_details=0',
        );
    }

    public function addUserPartListPart(int $partListId, Part $part, int $quantity): void
    {
        $this->doRequest(
            '/api/v3/users/'.$this->getUserToken().'/partlists/'.$partListId.'/parts/',
            Verb::POST,
            [
                'form_params' => [
                    'part_num' => $part->getPartNumber(),
                    'color_id' => $part->getColor()->getId(),
                    'quantity' => $quantity,
                ],
            ],
        );
    }

    public function removeUserPartListPart(int $partListId, Part $part): void
    {
        $this->doRequest(
            '/api/v3/users/'.$this->getUserToken().'/partlists/'.$partListId.'/parts/'.$part->getPartNumber().'/'.$part->getColor()->getId().'/',
            Verb::DELETE,
        );
    }

    public function updateUserPartListPart(int $partListId, Part $part, int $quantity): void
    {
        $this->doRequest(
            '/api/v3/users/'.$this->getUserToken().'/partlists/'.$partListId.'/parts/'.$part->getPartNumber().'/'.$part->getColor()->getId().'/',
            Verb::PUT,
            [
                'form_params' => [
                    'quantity' => $quantity,
                ],
            ],
        );
    }

    private function getUserToken(): string
    {
        static $userToken = null;

        if ($userToken === null) {
            $userToken = $this->doRequest(
                '/api/v3/users/_token/',
                Verb::POST,
                [
                    'form_params' => [
                        'username' => $this->username,
                        'password' => $this->password,
                    ],
                ]
            )['user_token'];
        }

        return $userToken;
    }
}
