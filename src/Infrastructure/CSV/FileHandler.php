<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\CSV;

class FileHandler
{
    public const FILE_EXTENSION = '.csv';

    private string $fileName;
    private string $filePath;

    public function __construct(
        string $csvFilePath
    ) {
        if (!str_ends_with($csvFilePath, self::FILE_EXTENSION)) {
            throw new \InvalidArgumentException('Given path is not a valid csv file.');
        }

        $this->fileName = basename($csvFilePath, self::FILE_EXTENSION);
        $this->filePath = $csvFilePath;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return \Generator<array<int, string>>
     */
    public function read(): \Generator
    {
        if (!is_file($this->filePath)) {
            throw new \InvalidArgumentException('Given file path does not exist.');
        }

        $handle = fopen($this->filePath, 'r');
        if ($handle === false) {
            throw new \RuntimeException('Could not read csv file "'.$this->filePath.'".');
        }

        while (($data = fgetcsv($handle)) !== false) {
            yield $data;
        }

        fclose($handle);
    }

    /**
     * @param \Generator<array<int, string>> $dataGenerator
     */
    public function write(\Generator $dataGenerator): void
    {
        $handle = fopen($this->filePath, 'w');
        if ($handle === false) {
            throw new \RuntimeException('Could not open or create csv file "'.$this->filePath.'".');
        }

        foreach ($dataGenerator as $data) {
            fputcsv($handle, $data);
        }

        fclose($handle);
    }
}
