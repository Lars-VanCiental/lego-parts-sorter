<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\CSV;

class DirectoryHandler
{
    private string $directoryPath;

    public function __construct(string $directoryPath)
    {
        if (!is_dir($directoryPath)) {
            throw new \InvalidArgumentException('Given path is not a valid directory.');
        }

        $this->directoryPath = $directoryPath;
    }

    /**
     * @return \Generator<int, FileHandler, void, void>
     */
    public function getFiles(): \Generator
    {
        $csvFiles = glob($this->directoryPath.'*.csv');
        if ($csvFiles === false) {
            throw new \RuntimeException('Could not find csv files in "'.$this->directoryPath.'".');
        }

        foreach ($csvFiles as $csvFilePath) {
            yield new FileHandler($csvFilePath);
        }
    }
}
