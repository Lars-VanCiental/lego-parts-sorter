<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Http;

interface Client
{
    /**
     * @param array<mixed> $options
     *
     * @return array<mixed>
     */
    public function doRequest(
        string $endpoint,
        Verb $verb,
        array $options,
    ): array;
}
