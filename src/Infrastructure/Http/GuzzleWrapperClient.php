<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Http;

use GuzzleHttp;

class GuzzleWrapperClient implements Client
{
    private GuzzleHttp\Client $client;

    public function __construct(
        GuzzleHttp\Client $client,
    ) {
        $this->client = $client;
    }

    /**
     * @param array<mixed> $options
     *
     * @return array<mixed>
     */
    public function doRequest(
        string $endpoint,
        Verb $verb,
        array $options,
    ): array {
        $response = $this->client->request(
            $verb->value,
            $endpoint,
            $options,
        );

        $content = $response->getBody()->getContents();

        if ($response->getStatusCode() === 204 || empty($content)) {
            return [];
        }

        $data = GuzzleHttp\Utils::jsonDecode($content, true);
        if (!is_array($data)) {
            throw new \RuntimeException('Request must return an array.');
        }

        return $data;
    }
}
