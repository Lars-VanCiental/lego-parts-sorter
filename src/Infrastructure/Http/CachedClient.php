<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Http;

use GuzzleHttp;

class CachedClient implements Client
{
    private readonly ?string $cacheDirectoryPath;
    private Client $client;

    public function __construct(
        ?string $cacheDirectoryPath,
        Client $client,
    ) {
        if ($cacheDirectoryPath !== null && !is_dir($cacheDirectoryPath)) {
            throw new \InvalidArgumentException('Invalid cache directory given: '.$cacheDirectoryPath);
        }

        $this->cacheDirectoryPath = $cacheDirectoryPath;
        $this->client = $client;
    }

    /**
     * @param array<mixed> $options
     *
     * @return array<mixed>
     */
    public function doRequest(
        string $endpoint,
        Verb $verb,
        array $options,
    ): array {
        $data = $this->readFromCache($endpoint);
        if ($data === null) {
            $data = $this->client->doRequest(
                $endpoint,
                $verb,
                $options,
            );

            $this->saveToCache($endpoint, $data);
        }

        return $data;
    }

    /**
     * @return ?array<mixed>
     */
    private function readFromCache(string $endpoint): ?array
    {
        if ($this->cacheDirectoryPath === null) {
            return null;
        }

        $cacheFilePath = $this->getCacheFile($endpoint);
        if (!file_exists($cacheFilePath)) {
            return null;
        }

        $content = file_get_contents($cacheFilePath);
        if ($content === false) {
            return null;
        }

        $data = GuzzleHttp\Utils::jsonDecode($content, true);
        if (!is_array($data)) {
            return null;
        }

        return $data;
    }

    /**
     * @param array<mixed> $data
     */
    private function saveToCache(string $endpoint, array $data): void
    {
        if ($this->cacheDirectoryPath === null) {
            return;
        }

        file_put_contents($this->getCacheFile($endpoint), GuzzleHttp\Utils::jsonEncode($data));
    }

    private function getCacheFile(string $endpoint): string
    {
        return $this->cacheDirectoryPath.DIRECTORY_SEPARATOR.sha1($endpoint).'.json';
    }
}
