<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure\Http;

enum Verb: string
{
    case DELETE = 'DELETE';
    case GET = 'GET';
    case POST = 'POST';
    case PUT = 'PUT';
}
