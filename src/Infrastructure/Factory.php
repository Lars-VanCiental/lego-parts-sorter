<?php

declare(strict_types=1);

namespace LVC\LegoSorter\Infrastructure;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Infrastructure\Adapter\CachedCatalog;
use LVC\LegoSorter\Infrastructure\Adapter\CSVCatalog;
use LVC\LegoSorter\Infrastructure\Adapter\CSVStorageSpace;
use LVC\LegoSorter\Infrastructure\Adapter\RebrickableCatalog;
use LVC\LegoSorter\Infrastructure\Adapter\RebrickableStorageSpace;
use LVC\LegoSorter\Infrastructure\Clients\Rebrickable;

class Factory
{
    /**
     * @param array<mixed> $configuration
     *
     * @return array{0: ?Catalog, 1: StorageSpace, 2: Matcher}
     */
    public static function fromConfiguration(array $configuration): array
    {
        $rebrickableClients = array_key_exists('rebrickable', $configuration) ? Rebrickable\ClientFactory::buildClients($configuration) : null;

        return [
            self::buildCatalog(
                $configuration['catalog'] ?? [],
                $rebrickableClients,
            ),
            self::buildStorageSpace(
                $configuration['storages'] ?? [],
                $rebrickableClients
            ),
            self::buildMatcher(
                $configuration['ignored_parts']['matcher'] ?? ['rule' => 'null'],
            ),
        ];
    }

    /**
     * @param mixed                                                                 $catalogConfiguration
     * @param ?array{0: Rebrickable\CatalogueClient, 1: ?Rebrickable\StorageClient} $rebrickableClients
     */
    private static function buildCatalog(
        $catalogConfiguration,
        ?array $rebrickableClients,
    ): ?Catalog {
        if (empty($catalogConfiguration) || !is_array($catalogConfiguration)) {
            return null;
        }

        $providerName = array_key_first($catalogConfiguration['provider']);
        if ($providerName === null || is_int($providerName)) {
            throw new \InvalidArgumentException('Invalid provider name given for catalog.');
        }

        if ($providerName === 'rebrickable') {
            if ($rebrickableClients === null) {
                throw new \InvalidArgumentException('You must configure a rebrickable client in order to use it as catalog.');
            }

            $rebrickableCatalogueClient = $rebrickableClients[0];
        }

        $providerConfiguration = $catalogConfiguration['provider'][$providerName];

        $catalog = match ($providerName) {
            'csv' => new CSVCatalog(
                $providerConfiguration['path'],
            ),
            'brickset' => null,
            'rebrickable' => new RebrickableCatalog($rebrickableCatalogueClient),
            default => null,
        };

        if ($catalog === null) {
            throw new \InvalidArgumentException('Unknown provider "'.$providerName.'" given to build catalog.');
        }

        return new CachedCatalog($catalog);
    }

    /**
     * @param mixed                                                                 $storageSpaceConfiguration
     * @param ?array{0: Rebrickable\CatalogueClient, 1: ?Rebrickable\StorageClient} $rebrickableClients
     */
    private static function buildStorageSpace(
        $storageSpaceConfiguration,
        ?array $rebrickableClients
    ): StorageSpace {
        if (!is_array($storageSpaceConfiguration)) {
            throw new \InvalidArgumentException('Invalid storage space configuration given.');
        }

        $storagesConfigurations = self::parseStoragesConfigurations($storageSpaceConfiguration['matching_rules'] ?? []);

        $providerName = array_key_first($storageSpaceConfiguration['provider']);
        if ($providerName === null || is_int($providerName)) {
            throw new \InvalidArgumentException('Invalid provider name given for storage.');
        }

        if ($providerName === 'rebrickable') {
            if (($rebrickableClients === null) || ($rebrickableClients[1] === null)) {
                throw new \InvalidArgumentException('You must configure a rebrickable client with a username and password in order to use it as storage space.');
            }

            $rebrickableCatalogueClient = $rebrickableClients[0];
            $rebrickableStorageClient = $rebrickableClients[1];
        }

        $providerConfiguration = $storageSpaceConfiguration['provider'][$providerName];

        $storageSpace = match ($providerName) {
            'csv' => new CSVStorageSpace(
                $providerConfiguration['path'],
                $storagesConfigurations
            ),
            'rebrickable' => new RebrickableStorageSpace(
                $rebrickableCatalogueClient,
                $rebrickableStorageClient,
                $storagesConfigurations
            ),
            default => null,
        };

        if ($storageSpace === null) {
            throw new \InvalidArgumentException('Unknown provider "'.$providerName.'" given to build storage space.');
        }

        return $storageSpace;
    }

    /**
     * @param mixed $storagesConfiguration
     *
     * @return array<string, array{string, Matcher}>
     */
    private static function parseStoragesConfigurations($storagesConfiguration): array
    {
        if (!is_array($storagesConfiguration)) {
            return [];
        }

        $storagesConfigurations = [];

        foreach ($storagesConfiguration as $storageConfiguration) {
            /** @var string $storageName */
            $storageName = $storageConfiguration['name'];
            $storagesConfigurations[$storageName] = [
                $storageConfiguration['description'],
                self::buildMatcher($storageConfiguration['matcher']),
            ];
        }

        return $storagesConfigurations;
    }

    /**
     * @param mixed $matcherConfiguration
     */
    private static function buildMatcher($matcherConfiguration): Matcher
    {
        if (!is_array($matcherConfiguration)) {
            throw new \InvalidArgumentException('Invalid  matcher configuration given.');
        }

        $matcher = match ($matcherConfiguration['rule'] ?? null) {
            'and' => new Matcher\AndMatcher(
                ...array_map([self::class, 'buildMatcher'], $matcherConfiguration['matchers'] ?? [])
            ),
            'category' => new Matcher\CategoryMatcher($matcherConfiguration['value']),
            'color' => new Matcher\ColorMatcher($matcherConfiguration['value']),
            'default' => new Matcher\DefaultMatcher(),
            'null' => new Matcher\NullMatcher(),
            'or' => new Matcher\OrMatcher(
                ...array_map([self::class, 'buildMatcher'], $matcherConfiguration['matchers'])
            ),
            'number' => new Matcher\PartNumberMatcher($matcherConfiguration['value']),
            'type' => new Matcher\TypeMatcher($matcherConfiguration['value']),
            default => null,
        };

        if ($matcher === null) {
            throw new \InvalidArgumentException('Unknown matcher rule "'.$matcherConfiguration['rule'].'" given to build the matcher.');
        }

        return $matcher;
    }
}
