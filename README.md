# README #

Ahoy and welcome to my lego parts sorter!

This project's idea came up as I was trying to sort all my Lego parts into different storages without ever remembering how and where to put them.
This tool help define a sorting configuration by showing which part belongs to nowhere, and tells you where to store each Lego part. 

## Installation ##

This project is a CLI tool working on PHP.
```shell
composer install
```

To see the commands available, you can use the following base command.
```shell
bin/console.php
```

## Main usage ##

_See configuration below for how to configure the sorter._

The main usage for the sorter, is to generate a report telling you where to put each part.
```shell
 bin/console.php sort:sets 6075 --draft -f html -o report
```

* You can sort multiple set at one time
* The `--draft` option will stop the command after the generation of the report without asking for save
* You can choose between CLI report or html report with `-f` option
* You can choose the name of the report file with `-o` when using the html report

Do not worry, the tool always ask for configuration before saving.

## Configuration ##

To start using the lego sorter, you will have to define your sorting configuration in a yaml file.

By default, the CLI tool will look for configuration files in the following folder `./var/configuration`

### Catalog ###

To retrieve Lego parts information, a catalog needs to be defined.
So far, you can use csv files to describe sets, or the rebrickable API.

### Storages ###

To save the sorting of your parts, you need to define a storages provider.
So far, you can use csv files to store your parts, or the rebrickable API.

### Matching rules ###

When configuring your storages, you will need to define matching rules. Only storages that have a matching rules defined will be considered for sorting.

A matching rule needs the name of the storage that it applies to, and a rule.

Available rules are the following:
* `and` completed with a list a `matchers`
* `or` completed with a list a `matchers`
* `category` completed with a `value`
* `color` completed with a `value`
* `type` completed with a `value`
* `default` accepts everything
* `null` rejects everything

### Ignored parts ###

You can also add an extra matcher configuration to ignore some parts during the sorting process.

### Example ###

```yaml
catalog:
  provider:
    rebrickable: true
storages:
  provider:
    csv:
      path: '~/lego-parts-sorter/var/csv/storages'
  matching_rules:
    -
      name: 'Box 1'
      description: 'Boats'
      matcher:
        rule: 'or'
        matchers:
          - rule: 'number'
            value: '54101'
          - rule: 'number'
            value: '62791'
          - rule: 'number'
            value: '18913'
          - rule: 'number'
            value: '18912'
    -
      name: 'Box 2'
      description: 'Rocks, Plants & Animals'
      matcher:
        rule: 'or'
        matchers:
          - rule: 'category'
            value: 'Rock'
          - rule: 'category'
            value: 'Plants and Animals'
    -
      name: 'Box 3'
      description: 'Red plates'
      matcher:
        rule: 'and'
        matchers:
          - rule: 'category'
            value: 'Plates'
          - rule: 'color'
            value: 'Red'
    -
      name: 'Box 4'
      description: 'Other parts'
      matcher:
        rule: 'default'
ignored_parts:
  matcher:
    rule: 'category'
    value: 'Stickers'
rebrickable:
  credential: 'somestringyouwillneed'
```

### Using CSV files ###

When using CSV files, the files must follow a pattern:
* Set files must be named `{set-number} - {set-name}`
* Storage files must be named according to the name given in the matching rule `{name}.csv`
* File's first line is ignored and should contains `PartNumber, Type, Color, Category`

For `6075-1 - Wolfpack Tower.csv`, the file should contain
```csv
PartNumber, Type, Color, Category
6020, Bar 7 x 3 with Double Clips (Ladder), Black, Bars\, Ladders and Fences
6020, Bar 7 x 3 with Double Clips (Ladder), Black, Bars\, Ladders and Fences
3005, Brick 1 x 1, Black, Bricks
3005, Brick 1 x 1, Black, Bricks
3005, Brick 1 x 1, Black, Bricks
3005, Brick 1 x 1, Black, Bricks
3005, Brick 1 x 1, Black, Bricks
3004, Brick 1 x 2, Black, Bricks
…
```

### Using Rebrickable ###

To use the rebrickable API, you will need to get a credential.

If you want to use it to store your parts, you will also need to configure your username and password to generate tokens.

```yaml
rebrickable:
  credential: 'something'
  username: 'Your-Pseudo'
  password: 'Your-Password'
```

## Contribution guidelines ##

Feel free to ask question, fork the repository, make suggestions, and even better, open pull request :)

For approval, a pull request must validate the different tools. You can run them with `composer check`.

### Known issues ###

* Need to add catalog implementation from brickset API.