#!/usr/bin/env php
<?php

require __DIR__.'/../vendor/autoload.php';

use LVC\LegoSorter\Infrastructure\Command\ConfigurationHelperCommand;
use LVC\LegoSorter\Infrastructure\Command\ConfigurationListCategoriesCommand;
use LVC\LegoSorter\Infrastructure\Command\ConfigurationListColorsCommand;
use LVC\LegoSorter\Infrastructure\Command\ConfigurationValidatorCommand;
use LVC\LegoSorter\Infrastructure\Command\EmptyStoragesCommand;
use LVC\LegoSorter\Infrastructure\Command\ReSortStoragesCommand;
use LVC\LegoSorter\Infrastructure\Command\SortSetsCommand;
use Symfony\Component\Console\Application;

$application = new Application('lego-parts-sorter');

$application->addCommands(
    [
        new ConfigurationHelperCommand(),
        new ConfigurationValidatorCommand(),
        new ConfigurationListCategoriesCommand(),
        new ConfigurationListColorsCommand(),
        new EmptyStoragesCommand(),
        new ReSortStoragesCommand(),
        new SortSetsCommand(),
    ]
);

$application->run();
