<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Category;
use LVC\LegoSorter\Domain\Lego\Color;
use LVC\LegoSorter\Domain\Lego\Part;

trait MatcherTestCasesHelper
{
    /**
     * @return array{unmatchedPartMock: Part, matchedPartNumberPartMock: Part, matchedTypePartMock: Part, matchedColorPartMock: Part, matchedCategoryPartMock: Part}
     */
    private function getPartMocks(): array
    {
        $unmatchedColorMock = $this->createMock(Color::class);
        $unmatchedColorMock->method('getName')->willReturn('unmatched');
        $matchedColorMock = $this->createMock(Color::class);
        $matchedColorMock->method('getName')->willReturn('matched');
        $unmatchedCategoryMock = $this->createMock(Category::class);
        $unmatchedCategoryMock->method('getName')->willReturn('unmatched');
        $matchedCategoryMock = $this->createMock(Category::class);
        $matchedCategoryMock->method('getName')->willReturn('matched');

        $unmatchedPartMock = $this->createMock(Part::class);
        $unmatchedPartMock->method('getPartNumber')->willReturn('unmatched');
        $unmatchedPartMock->method('getType')->willReturn('unmatched');
        $unmatchedPartMock->method('getColor')->willReturn($unmatchedColorMock);
        $unmatchedPartMock->method('getCategory')->willReturn($unmatchedCategoryMock);

        $matchedPartNumberPartMock = $this->createMock(Part::class);
        $matchedPartNumberPartMock->method('getPartNumber')->willReturn('matched');
        $matchedPartNumberPartMock->method('getType')->willReturn('unmatched');
        $matchedPartNumberPartMock->method('getColor')->willReturn($unmatchedColorMock);
        $matchedPartNumberPartMock->method('getCategory')->willReturn($unmatchedCategoryMock);

        $matchedTypePartMock = $this->createMock(Part::class);
        $matchedTypePartMock->method('getPartNumber')->willReturn('unmatched');
        $matchedTypePartMock->method('getType')->willReturn('matched');
        $matchedTypePartMock->method('getColor')->willReturn($unmatchedColorMock);
        $matchedTypePartMock->method('getCategory')->willReturn($unmatchedCategoryMock);

        $matchedColorPartMock = $this->createMock(Part::class);
        $matchedColorPartMock->method('getPartNumber')->willReturn('unmatched');
        $matchedColorPartMock->method('getType')->willReturn('unmatched');
        $matchedColorPartMock->method('getColor')->willReturn($matchedColorMock);
        $matchedColorPartMock->method('getCategory')->willReturn($unmatchedCategoryMock);

        $matchedCategoryPartMock = $this->createMock(Part::class);
        $matchedCategoryPartMock->method('getPartNumber')->willReturn('unmatched');
        $matchedCategoryPartMock->method('getType')->willReturn('unmatched');
        $matchedCategoryPartMock->method('getColor')->willReturn($unmatchedColorMock);
        $matchedCategoryPartMock->method('getCategory')->willReturn($matchedCategoryMock);

        return [
            'unmatchedPartMock' => $unmatchedPartMock,
            'matchedPartNumberPartMock' => $matchedPartNumberPartMock,
            'matchedTypePartMock' => $matchedTypePartMock,
            'matchedColorPartMock' => $matchedColorPartMock,
            'matchedCategoryPartMock' => $matchedCategoryPartMock,
        ];
    }
}
