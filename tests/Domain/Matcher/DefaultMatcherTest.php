<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher\DefaultMatcher;
use LVC\LegoSorterTests\TestCase;

class DefaultMatcherTest extends TestCase
{
    use MatcherTestCasesHelper;

    /**
     * @dataProvider getAcceptTestCases
     */
    public function testAccept(bool $expectedResult, Part $part): void
    {
        $matcher = new DefaultMatcher();

        self::assertSame($expectedResult, $matcher->accept($part));
    }

    /**
     * @return \Generator<array<int, mixed>>
     */
    public function getAcceptTestCases(): \Generator
    {
        [
            'unmatchedPartMock' => $unmatchedPartMock,
            'matchedPartNumberPartMock' => $matchedPartNumberPartMock,
            'matchedTypePartMock' => $matchedTypePartMock,
            'matchedColorPartMock' => $matchedColorPartMock,
            'matchedCategoryPartMock' => $matchedCategoryPartMock
        ] = $this->getPartMocks();

        yield 'all unmatched is accepted' => [
            true,
            $unmatchedPartMock,
        ];

        yield 'matched part number is accepted' => [
            true,
            $matchedPartNumberPartMock,
        ];

        yield 'matched type is accepted' => [
            true,
            $matchedTypePartMock,
        ];

        yield 'matched color is accepted' => [
            true,
            $matchedColorPartMock,
        ];

        yield 'matched category is accepted' => [
            true,
            $matchedCategoryPartMock,
        ];
    }
}
