<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Category;
use LVC\LegoSorter\Domain\Lego\Color;
use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Matcher\OrMatcher;
use LVC\LegoSorterTests\TestCase;

class OrMatcherTest extends TestCase
{
    /**
     * @dataProvider getAcceptTestCases
     */
    public function testAccept(bool $expectedResult, Part $part, Matcher ...$matchers): void
    {
        $matcher = new OrMatcher(...$matchers);

        self::assertSame($expectedResult, $matcher->accept($part));
    }

    /**
     * @return \Generator<array<int, mixed>>
     */
    public function getAcceptTestCases(): \Generator
    {
        $colorMock = $this->createMock(Color::class);
        $colorMock->method('getName')->willReturn('color');
        $categoryMock = $this->createMock(Category::class);
        $categoryMock->method('getName')->willReturn('category');

        $partMock = $this->createMock(Part::class);
        $partMock->method('getPartNumber')->willReturn('tested part');
        $partMock->method('getType')->willReturn('type');
        $partMock->method('getColor')->willReturn($colorMock);
        $partMock->method('getCategory')->willReturn($categoryMock);

        $unmatchedMatcherMock = $this->createMock(Matcher::class);
        $unmatchedMatcherMock->method('accept')->willReturn(false);

        $matchedMatcherMock = $this->createMock(Matcher::class);
        $matchedMatcherMock->method('accept')->willReturn(true);

        yield 'all unmatched is rejected' => [
            false,
            $partMock,
            $unmatchedMatcherMock,
            $unmatchedMatcherMock,
            $unmatchedMatcherMock,
        ];

        yield 'one matched is accepted' => [
            true,
            $partMock,
            $unmatchedMatcherMock,
            $matchedMatcherMock,
            $unmatchedMatcherMock,
        ];

        yield 'all matched is accepted' => [
            true,
            $partMock,
            $matchedMatcherMock,
            $matchedMatcherMock,
            $matchedMatcherMock,
        ];
    }
}
