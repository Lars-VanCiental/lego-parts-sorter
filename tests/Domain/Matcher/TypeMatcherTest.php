<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher\TypeMatcher;
use LVC\LegoSorterTests\TestCase;

class TypeMatcherTest extends TestCase
{
    use MatcherTestCasesHelper;

    /**
     * @dataProvider getAcceptTestCases
     */
    public function testAccept(bool $expectedResult, Part $part): void
    {
        $matcher = new TypeMatcher('matched');

        self::assertSame($expectedResult, $matcher->accept($part));
    }

    /**
     * @return \Generator<array<int, mixed>>
     */
    public function getAcceptTestCases(): \Generator
    {
        [
            'unmatchedPartMock' => $unmatchedPartMock,
            'matchedPartNumberPartMock' => $matchedPartNumberPartMock,
            'matchedTypePartMock' => $matchedTypePartMock,
            'matchedColorPartMock' => $matchedColorPartMock,
            'matchedCategoryPartMock' => $matchedCategoryPartMock
        ] = $this->getPartMocks();

        yield 'all unmatched is rejected' => [
            false,
            $unmatchedPartMock,
        ];

        yield 'matched part number is rejected' => [
            false,
            $matchedPartNumberPartMock,
        ];

        yield 'matched type is accepted' => [
            true,
            $matchedTypePartMock,
        ];

        yield 'matched color is rejected' => [
            false,
            $matchedColorPartMock,
        ];

        yield 'matched category is rejected' => [
            false,
            $matchedCategoryPartMock,
        ];
    }
}
