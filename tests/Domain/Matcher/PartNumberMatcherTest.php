<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Domain\Matcher;

use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Matcher\PartNumberMatcher;
use LVC\LegoSorterTests\TestCase;

class PartNumberMatcherTest extends TestCase
{
    use MatcherTestCasesHelper;

    /**
     * @dataProvider getAcceptTestCases
     */
    public function testAccept(bool $expectedResult, Part $part): void
    {
        $matcher = new PartNumberMatcher('matched');

        self::assertSame($expectedResult, $matcher->accept($part));
    }

    /**
     * @return \Generator<array<int, mixed>>
     */
    public function getAcceptTestCases(): \Generator
    {
        [
            'unmatchedPartMock' => $unmatchedPartMock,
            'matchedPartNumberPartMock' => $matchedPartNumberPartMock,
            'matchedTypePartMock' => $matchedTypePartMock,
            'matchedColorPartMock' => $matchedColorPartMock,
            'matchedCategoryPartMock' => $matchedCategoryPartMock
        ] = $this->getPartMocks();

        yield 'all unmatched is rejected' => [
            false,
            $unmatchedPartMock,
        ];

        yield 'matched part number is accepted' => [
            true,
            $matchedPartNumberPartMock,
        ];

        yield 'matched type is rejected' => [
            false,
            $matchedTypePartMock,
        ];

        yield 'matched color is rejected' => [
            false,
            $matchedColorPartMock,
        ];

        yield 'matched category is rejected' => [
            false,
            $matchedCategoryPartMock,
        ];
    }
}
