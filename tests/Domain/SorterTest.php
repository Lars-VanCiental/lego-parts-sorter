<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Domain;

use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Sorter;
use LVC\LegoSorter\Domain\SortingResult;
use LVC\LegoSorterTests\DomainMockHelper;
use LVC\LegoSorterTests\TestCase;

class SorterTest extends TestCase
{
    use DomainMockHelper;

    public function testSortWithoutAnyStoragePutsAllPartsInTheUnsortedPartsStack(): void
    {
        $ignoringMatcher = new Matcher\NullMatcher();
        $sortingResult = new SortingResult();

        $set = $this->getSetMock(
            '6097-1',
            'Night Lord\'s Castle',
            $part1 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1'),
            $part2 = $this->getPartMock('2', 'type-2', 'color#1', 'category/1'),
            $part3 = $this->getPartMock('3', 'type-3', 'color#1', 'category/2'),
            $part4 = $this->getPartMock('4', 'type-1', 'color#1', 'category/2'),
            $part5 = $this->getPartMock('5', 'type-2', 'color#2', 'category/2'),
        );

        $sorter = new Sorter($ignoringMatcher, $sortingResult);

        self::assertEmpty($sortingResult->getStoringPlans());
        self::assertEmpty($sortingResult->getUnsortedPartsStack()->getParts());

        $sorter->sortSet($set);

        self::assertEmpty($sortingResult->getStoringPlans());
        self::assertCount(5, $sortingResult->getUnsortedPartsStack()->getParts());
        self::assertSame(
            [
                $part1,
                $part2,
                $part3,
                $part4,
                $part5,
            ],
            $sortingResult->getUnsortedPartsStack()->getParts()
        );

        self::assertEmpty($sortingResult->getIgnoredPartsList()->getParts());
    }

    public function testSortWith2StorageSplitParts(): void
    {
        $ignoringMatcher = new Matcher\NullMatcher();
        $sortingResult = new SortingResult();
        $set = $this->getSetMock(
            '6560-1',
            'Diving Expedition Explorer',
            $part1 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1'),
            $part2 = $this->getPartMock('2', 'type-2', 'color#1', 'category/1'),
            $part3 = $this->getPartMock('3', 'type-3', 'color#1', 'category/2'),
            $part4 = $this->getPartMock('4', 'type-1', 'color#1', 'category/2'),
            $part5 = $this->getPartMock('5', 'type-2', 'color#2', 'category/2'),
        );

        $type1Storage = $this->getStorage(new Matcher\TypeMatcher('type-1'));
        $color1Category2Storage = $this->getStorage(new Matcher\AndMatcher(new Matcher\ColorMatcher('color#1'), new Matcher\CategoryMatcher('category/2')));

        $sorter = new Sorter($ignoringMatcher, $sortingResult, $type1Storage, $color1Category2Storage);

        self::assertEmpty($sortingResult->getStoringPlans());
        self::assertEmpty($sortingResult->getUnsortedPartsStack()->getParts());

        $sorter->sortSet($set);

        self::assertCount(3, $sortingResult->getStoringPlans());
        [$storingPlan1, $storingPlan2, $storingPlan3] = $sortingResult->getStoringPlans();

        self::assertSame($part1, $storingPlan1->getPart());
        self::assertSame($type1Storage, $storingPlan1->getStorage());

        self::assertSame($part3, $storingPlan2->getPart());
        self::assertSame($color1Category2Storage, $storingPlan2->getStorage());

        self::assertSame($part4, $storingPlan3->getPart());
        self::assertSame($type1Storage, $storingPlan3->getStorage());

        self::assertCount(2, $sortingResult->getUnsortedPartsStack()->getParts());
        self::assertSame(
            [
                $part2,
                $part5,
            ],
            $sortingResult->getUnsortedPartsStack()->getParts()
        );

        self::assertEmpty($sortingResult->getIgnoredPartsList()->getParts());
    }

    public function testSortPartPutInTheFirstMatchingStorage(): void
    {
        $ignoringMatcher = new Matcher\NullMatcher();
        $sortingResult = new SortingResult();

        $part = $this->getPartMock('1', 'type-1', 'color#1', 'category/1');

        $type1Storage1 = $this->getStorage(new Matcher\TypeMatcher('type-1'));
        $type1Storage2 = $this->getStorage(new Matcher\TypeMatcher('type-1'));

        $sorter = new Sorter($ignoringMatcher, $sortingResult, $type1Storage1, $type1Storage2);

        self::assertEmpty($type1Storage1->getParts());
        self::assertEmpty($type1Storage2->getParts());
        self::assertEmpty($sortingResult->getStoringPlans());
        self::assertEmpty($sortingResult->getUnsortedPartsStack()->getParts());

        $sorter->sortPart($part);

        self::assertCount(1, $sortingResult->getStoringPlans());
        [$storingPlan] = $sortingResult->getStoringPlans();

        self::assertSame($part, $storingPlan->getPart());
        self::assertSame($type1Storage1, $storingPlan->getStorage());

        self::assertEmpty($sortingResult->getUnsortedPartsStack()->getParts());

        self::assertEmpty($sortingResult->getIgnoredPartsList()->getParts());
    }

    public function testSortPartIgnoreMAtchingParts(): void
    {
        $ignoringMatcher = new Matcher\CategoryMatcher('category/1');
        $sortingResult = new SortingResult();

        $set = $this->getSetMock(
            '6032-1',
            'Catapult Crusher',
            $part1 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1'),
            $part2 = $this->getPartMock('2', 'type-2', 'color#1', 'category/1'),
            $part3 = $this->getPartMock('3', 'type-3', 'color#1', 'category/2'),
            $part4 = $this->getPartMock('4', 'type-1', 'color#1', 'category/2'),
            $part5 = $this->getPartMock('5', 'type-2', 'color#2', 'category/2'),
        );

        $type1Storage = $this->getStorage(new Matcher\TypeMatcher('type-1'));
        $color1Category2Storage = $this->getStorage(new Matcher\AndMatcher(new Matcher\ColorMatcher('color#1'), new Matcher\CategoryMatcher('category/2')));

        $sorter = new Sorter($ignoringMatcher, $sortingResult, $type1Storage, $color1Category2Storage);

        self::assertEmpty($sortingResult->getStoringPlans());
        self::assertEmpty($sortingResult->getUnsortedPartsStack()->getParts());

        $sorter->sortSet($set);

        self::assertCount(2, $sortingResult->getStoringPlans());
        [$storingPlan1, $storingPlan2] = $sortingResult->getStoringPlans();

        self::assertSame($part3, $storingPlan1->getPart());
        self::assertSame($color1Category2Storage, $storingPlan1->getStorage());

        self::assertSame($part4, $storingPlan2->getPart());
        self::assertSame($type1Storage, $storingPlan2->getStorage());

        self::assertCount(1, $sortingResult->getUnsortedPartsStack()->getParts());
        self::assertSame(
            [
                $part5,
            ],
            $sortingResult->getUnsortedPartsStack()->getParts()
        );

        self::assertCount(2, $sortingResult->getIgnoredPartsList()->getParts());
        self::assertSame(
            [
                $part1,
                $part2,
            ],
            $sortingResult->getIgnoredPartsList()->getParts()
        );
    }
}
