<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Application;

use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Application\StoreSortedPartsAction;
use LVC\LegoSorter\Domain\Matcher\NullMatcher;
use LVC\LegoSorter\Domain\SortingResult;
use LVC\LegoSorterTests\DomainMockHelper;
use LVC\LegoSorterTests\TestCase;

class StoreSortedPartsActionTest extends TestCase
{
    use DomainMockHelper;

    public function testStorageFailedWhenPlanIncludeUnknownStorage(): void
    {
        $part1 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1');
        $part2 = $this->getPartMock('2', 'type-2', 'color#1', 'category/2');

        $knownStorage = $this->getStorage(new NullMatcher());
        $unknownStorage = $this->getStorage(new NullMatcher());

        $sortingResult = new SortingResult();
        $sortingResult->planPartStorage($part1, $knownStorage);
        $sortingResult->planPartStorage($part2, $unknownStorage);

        $storageSpaceMock = $this->createMock(StorageSpace::class);
        $storageSpaceMock->method('takeOutStorages')->willReturn([$knownStorage]);
        $storageSpaceMock->expects(self::never())->method('putAwayStorages');

        self::expectException(\RuntimeException::class);
        self::expectExceptionMessage('Storage plan given toward a missing storage "'.$unknownStorage->getName().'"');

        $emptyStoragesAction = new StoreSortedPartsAction($storageSpaceMock);
        $unsortedPartsStack = $emptyStoragesAction($sortingResult, false);
    }

    public function testStoragesAreSavedAfterPlanIsApplied(): void
    {
        $part111 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1');
        $part121 = $this->getPartMock('2', 'type-1', 'color#2', 'category/1');
        $part333 = $this->getPartMock('3', 'type-3', 'color#3', 'category/3');
        $part311 = $this->getPartMock('4', 'type-3', 'color#1', 'category/1');
        $part322 = $this->getPartMock('5', 'type-3', 'color#2', 'category/2');
        $part113 = $this->getPartMock('6', 'type-1', 'color#1', 'category/3');
        $part312 = $this->getPartMock('7', 'type-3', 'color#1', 'category/2');
        $part223 = $this->getPartMock('8', 'type-2', 'color#2', 'category/3');
        $part231 = $this->getPartMock('9', 'type-2', 'color#3', 'category/1');

        $storage1 = $this->getStorage(new NullMatcher());
        $storage1->storePart($part111);
        $storage2 = $this->getStorage(new NullMatcher());
        $storage2->storePart($part121);
        $storage2->storePart($part333);

        $sortingResult = new SortingResult();
        $sortingResult->planPartStorage($part311, $storage1);
        $sortingResult->planPartStorage($part322, $storage1);
        $sortingResult->planPartStorage($part113, $storage2);
        $sortingResult->planPartStorage($part312, $storage2);
        $sortingResult->stackPart($part223);
        $sortingResult->stackPart($part231);

        $storageSpaceMock = $this->createMock(StorageSpace::class);
        $storageSpaceMock->method('takeOutStorages')->willReturn([$storage1, $storage2]);
        $storageSpaceMock->expects(self::once())->method('putAwayStorages')->with($storage1, $storage2);

        self::assertCount(1, $storage1->getParts());
        self::assertSame([$part111], $storage1->getParts());
        self::assertCount(2, $storage2->getParts());
        self::assertSame([$part121, $part333], $storage2->getParts());

        $emptyStoragesAction = new StoreSortedPartsAction($storageSpaceMock);
        $unsortedPartsStack = $emptyStoragesAction($sortingResult, false);

        self::assertCount(3, $storage1->getParts());
        self::assertSame([$part111, $part311, $part322], $storage1->getParts());
        self::assertCount(4, $storage2->getParts());
        self::assertSame([$part121, $part333, $part113, $part312], $storage2->getParts());
        self::assertCount(2, $unsortedPartsStack->getParts());
        self::assertSame([$part223, $part231], $unsortedPartsStack->getParts());
    }

    public function testStoragesCanBeEmptiedFirst(): void
    {
        $part111 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1');
        $part121 = $this->getPartMock('2', 'type-1', 'color#2', 'category/1');
        $part333 = $this->getPartMock('3', 'type-3', 'color#3', 'category/3');
        $part311 = $this->getPartMock('4', 'type-3', 'color#1', 'category/1');
        $part322 = $this->getPartMock('5', 'type-3', 'color#2', 'category/2');
        $part113 = $this->getPartMock('6', 'type-1', 'color#1', 'category/3');
        $part312 = $this->getPartMock('7', 'type-3', 'color#1', 'category/2');
        $part223 = $this->getPartMock('8', 'type-2', 'color#2', 'category/3');
        $part231 = $this->getPartMock('9', 'type-2', 'color#3', 'category/1');

        $storage1 = $this->getStorage(new NullMatcher());
        $storage1->storePart($part111);
        $storage2 = $this->getStorage(new NullMatcher());
        $storage2->storePart($part121);
        $storage2->storePart($part333);

        $sortingResult = new SortingResult();
        $sortingResult->planPartStorage($part311, $storage1);
        $sortingResult->planPartStorage($part322, $storage1);
        $sortingResult->planPartStorage($part113, $storage2);
        $sortingResult->planPartStorage($part312, $storage2);
        $sortingResult->stackPart($part223);
        $sortingResult->stackPart($part231);

        $storageSpaceMock = $this->createMock(StorageSpace::class);
        $storageSpaceMock->method('takeOutStorages')->willReturn([$storage1, $storage2]);
        $storageSpaceMock->expects(self::once())->method('putAwayStorages')->with($storage1, $storage2);

        self::assertCount(1, $storage1->getParts());
        self::assertSame([$part111], $storage1->getParts());
        self::assertCount(2, $storage2->getParts());
        self::assertSame([$part121, $part333], $storage2->getParts());

        $emptyStoragesAction = new StoreSortedPartsAction($storageSpaceMock);
        $unsortedPartsStack = $emptyStoragesAction($sortingResult, true);

        self::assertCount(2, $storage1->getParts());
        self::assertSame([$part311, $part322], $storage1->getParts());
        self::assertCount(2, $storage2->getParts());
        self::assertSame([$part113, $part312], $storage2->getParts());
        self::assertCount(5, $unsortedPartsStack->getParts());
        self::assertSame([$part223, $part231, $part111, $part121, $part333], $unsortedPartsStack->getParts());
    }
}
