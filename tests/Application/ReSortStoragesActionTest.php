<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Application;

use LVC\LegoSorter\Application\ReSortStoragesAction;
use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Domain\Matcher\CategoryMatcher;
use LVC\LegoSorter\Domain\Matcher\ColorMatcher;
use LVC\LegoSorterTests\DomainMockHelper;
use LVC\LegoSorterTests\TestCase;

class ReSortStoragesActionTest extends TestCase
{
    use DomainMockHelper;

    public function testStoragesAreNotSavedAfterSort(): void
    {
        $part111 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1');
        $part121 = $this->getPartMock('2', 'type-1', 'color#2', 'category/1');
        $part333 = $this->getPartMock('3', 'type-3', 'color#3', 'category/3');
        $part311 = $this->getPartMock('4', 'type-3', 'color#1', 'category/1');
        $part322 = $this->getPartMock('5', 'type-3', 'color#2', 'category/2');
        $part113 = $this->getPartMock('6', 'type-1', 'color#1', 'category/3');

        $storage1 = $this->getStorage(new ColorMatcher('color#2'));
        $storage1->storePart($part111);
        $storage1->storePart($part121);
        $storage1->storePart($part333);
        $storage2 = $this->getStorage(new CategoryMatcher('category/1'));
        $storage2->storePart($part311);
        $storage2->storePart($part322);
        $storage2->storePart($part113);

        $storageSpaceMock = $this->createMock(StorageSpace::class);
        $storageSpaceMock->method('takeOutStorages')->willReturn([$storage1, $storage2]);
        $storageSpaceMock->expects(self::never())->method('putAwayStorages');

        self::assertCount(3, $storage1->getParts());
        self::assertSame(
            [$part111, $part121, $part333],
            $storage1->getParts()
        );
        self::assertCount(3, $storage2->getParts());
        self::assertSame(
            [$part311, $part322, $part113],
            $storage2->getParts()
        );

        $reSortStoragesAction = new ReSortStoragesAction($storageSpaceMock);
        $sortingResult = $reSortStoragesAction();

        self::assertCount(3, $storage1->getParts());
        self::assertSame(
            [$part111, $part121, $part333],
            $storage1->getParts()
        );
        self::assertCount(3, $storage2->getParts());
        self::assertSame(
            [$part311, $part322, $part113],
            $storage2->getParts()
        );

        self::assertCount(4, $sortingResult->getStoringPlans());
        [$storingPlan1, $storingPlan2, $storingPlan3, $storingPlan4] = $sortingResult->getStoringPlans();
        self::assertSame($part111, $storingPlan1->getPart());
        self::assertSame($storage2, $storingPlan1->getStorage());
        self::assertSame($part121, $storingPlan2->getPart());
        self::assertSame($storage1, $storingPlan2->getStorage());
        self::assertSame($part311, $storingPlan3->getPart());
        self::assertSame($storage2, $storingPlan3->getStorage());
        self::assertSame($part322, $storingPlan4->getPart());
        self::assertSame($storage1, $storingPlan4->getStorage());
        self::assertCount(2, $sortingResult->getUnsortedPartsStack()->getParts());
        self::assertSame(
            [$part333, $part113],
            $sortingResult->getUnsortedPartsStack()->getParts()
        );
    }
}
