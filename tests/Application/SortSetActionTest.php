<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests\Application;

use LVC\LegoSorter\Application\Catalog;
use LVC\LegoSorter\Application\SortSetAction;
use LVC\LegoSorter\Application\StorageSpace;
use LVC\LegoSorter\Domain\Lego\Set;
use LVC\LegoSorter\Domain\Matcher\CategoryMatcher;
use LVC\LegoSorter\Domain\Matcher\ColorMatcher;
use LVC\LegoSorter\Domain\Matcher\NullMatcher;
use LVC\LegoSorterTests\DomainMockHelper;
use LVC\LegoSorterTests\TestCase;

class SortSetActionTest extends TestCase
{
    use DomainMockHelper;

    public function testUnknownSetGivesAnExceptionWhenConfiguredFor(): void
    {
        $catalogMock = $this->createMock(Catalog::class);
        $catalogMock->method('getSet')->willReturn(null);

        $storage1 = $this->getStorage(new NullMatcher());
        $storage2 = $this->getStorage(new NullMatcher());

        $storageSpaceMock = $this->createMock(StorageSpace::class);
        $storageSpaceMock->method('takeOutStorages')->willReturn([$storage1, $storage2]);
        $storageSpaceMock->expects(self::never())->method('putAwayStorages');

        $sortSetAction = new SortSetAction(new NullMatcher(), $storageSpaceMock, $catalogMock, false);

        self::expectException(\UnexpectedValueException::class);
        self::expectExceptionMessage('No set found with given set number "0".');

        $sortSetAction('0', '1');
    }

    public function testUnknownSetAreIgnoredWhenConfiguredFor(): void
    {
        $set = $this->getSetMock(
            '60194',
            'Arctic Scout Truck',
            $part1 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1'),
            $part2 = $this->getPartMock('2', 'type-2', 'color#1', 'category/2'),
            $part3 = $this->getPartMock('3', 'type-3', 'color#2', 'category/2'),
        );
        $catalogMock = $this->createMock(Catalog::class);
        $catalogMock->method('getSet')->willReturnCallback(fn (string $setId): ?Set => $setId === '1' ? null : $set);

        $storage1 = $this->getStorage(new NullMatcher());
        $storage2 = $this->getStorage(new NullMatcher());

        $storageSpaceMock = $this->createMock(StorageSpace::class);
        $storageSpaceMock->method('takeOutStorages')->willReturn([$storage1, $storage2]);
        $storageSpaceMock->expects(self::never())->method('putAwayStorages');

        $sortSetAction = new SortSetAction(new NullMatcher(), $storageSpaceMock, $catalogMock, true);

        $sortingResult = $sortSetAction('0', '1', '2');

        self::assertEmpty($sortingResult->getStoringPlans());
        self::assertCount(6, $sortingResult->getUnsortedPartsStack()->getParts());
        self::assertSame([$part1, $part2, $part3, $part1, $part2, $part3], $sortingResult->getUnsortedPartsStack()->getParts());
    }

    public function testStoragesAreNotSavedAfterSort(): void
    {
        $set = $this->getSetMock(
            '60194',
            'Arctic Scout Truck',
            $part1 = $this->getPartMock('1', 'type-1', 'color#1', 'category/1'),
            $part2 = $this->getPartMock('2', 'type-2', 'color#1', 'category/2'),
            $part3 = $this->getPartMock('3', 'type-3', 'color#2', 'category/2'),
        );

        $catalogMock = $this->createMock(Catalog::class);
        $catalogMock->method('getSet')->willReturn($set);

        $storage1 = $this->getStorage(new CategoryMatcher('category/1'));
        $storage2 = $this->getStorage(new ColorMatcher('color#2'));

        $storageSpaceMock = $this->createMock(StorageSpace::class);
        $storageSpaceMock->method('takeOutStorages')->willReturn([$storage1, $storage2]);
        $storageSpaceMock->expects(self::never())->method('putAwayStorages');

        $sortSetAction = new SortSetAction(new NullMatcher(), $storageSpaceMock, $catalogMock, false);
        $sortingResult = $sortSetAction('60194');

        self::assertCount(2, $sortingResult->getStoringPlans());
        [$storingPlan1, $storingPlan2] = $sortingResult->getStoringPlans();
        self::assertSame($part1, $storingPlan1->getPart());
        self::assertSame($storage1, $storingPlan1->getStorage());
        self::assertSame($part3, $storingPlan2->getPart());
        self::assertSame($storage2, $storingPlan2->getStorage());
        self::assertCount(1, $sortingResult->getUnsortedPartsStack()->getParts());
        self::assertSame([$part2], $sortingResult->getUnsortedPartsStack()->getParts());
    }
}
