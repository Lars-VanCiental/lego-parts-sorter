<?php

declare(strict_types=1);

namespace LVC\LegoSorterTests;

use LVC\LegoSorter\Domain\Lego\Category;
use LVC\LegoSorter\Domain\Lego\Color;
use LVC\LegoSorter\Domain\Lego\Part;
use LVC\LegoSorter\Domain\Lego\Set;
use LVC\LegoSorter\Domain\Matcher;
use LVC\LegoSorter\Domain\Storage;
use LVC\LegoSorter\Domain\UnsortedPartsStack;

trait DomainMockHelper
{
    private function getSetMock(string $setNumber, string $name, Part ...$parts): Set
    {
        $setMock = $this->createMock(Set::class);
        $setMock->method('getSetNumber')->willReturn($setNumber);
        $setMock->method('getName')->willReturn($name);
        $setMock->method('getParts')->willReturn($parts);

        return $setMock;
    }

    private function getPartMock(string $partNumber, string $type, string $color, string $category): Part
    {
        $colorMock = $this->createMock(Color::class);
        $colorMock->method('getName')->willReturn($color);
        $categoryMock = $this->createMock(Category::class);
        $categoryMock->method('getName')->willReturn($category);

        $partMock = $this->createMock(Part::class);
        $partMock->method('getPartNumber')->willReturn($partNumber);
        $partMock->method('getType')->willReturn($type);
        $partMock->method('getColor')->willReturn($colorMock);
        $partMock->method('getCategory')->willReturn($categoryMock);

        return $partMock;
    }

    private function getStorage(Matcher $matcher): Storage
    {
        return new class($matcher) implements Storage {
            private Matcher $matcher;
            /** @var Part[] */
            private array $parts = [];

            public function __construct(Matcher $matcher)
            {
                $this->matcher = $matcher;
            }

            public function getName(): string
            {
                return 'test.'.spl_object_hash($this);
            }

            public function getDescription(): string
            {
                return 'Test '.spl_object_hash($this);
            }

            public function getMatcher(): Matcher
            {
                return $this->matcher;
            }

            public function empty(): UnsortedPartsStack
            {
                $unsortedPartsStack = new UnsortedPartsStack();

                foreach ($this->parts as $part) {
                    $unsortedPartsStack->stackPart($part);
                }

                $this->parts = [];

                return $unsortedPartsStack;
            }

            public function storePart(Part $part): void
            {
                $this->parts[] = $part;
            }

            public function getParts(): array
            {
                return $this->parts;
            }
        };
    }
}
